<?php
/**
 * Created by PhpStorm.
 * User: James Bond
 * Date: 7/4/2017
 * Time: 9:45 PM
 */

namespace App\model;
if(!isset($_SESSION) )  session_start();
use App\database\Database;
use App\Utility\Utility;
use PDO;
use App\Message\Message;

class Opening_product extends Database
{
    public $id;
    public $prod_id;
    public $quantity;
    public $buy_price;
    public $date;
    public $admin_id;

    public function __construct(){
        parent::__construct();
    }

    public function prepareData($data){
        if (array_key_exists('id', $data)) {
            $this->id = $data['id'];
        }
        if (array_key_exists('product_name', $data)) {
            $this->prod_id = $data['product_name'];
        }
        if (array_key_exists('quantity', $data)) {
            $this->quantity = $data['quantity'];
        }
        if (array_key_exists('price', $data)) {
            $this->buy_price = $data['price'];
        }
        if (array_key_exists('admin_id', $data)) {
            $this->admin_id = $data['admin_id'];
        }

        return $this;

    }
    public function store(){
        date_default_timezone_set('Asia/Dhaka');
        $date = date('Y-m-d H:i:s');
        $this->date=$date;
        $query= "INSERT INTO `opening_product`(admin_id,prod_id,quantity,buy_price,date) VALUES (?,?,?,?,?)";

        $STH = $this->DBH->prepare($query);
        $STH->bindParam(1,$this->admin_id);
        $STH->bindParam(2,$this->prod_id);
        $STH->bindParam(3,$this->quantity);
        $STH->bindParam(4,$this->buy_price);
        $STH->bindParam(5,$this->date);

        $result = $STH->execute();
        if($result){

            Message::setMessage("Success! Opening Product has been added");
        }
        else{
            Message::setMessage("Failed! data has not be inserted!");
        }
    }
    public function update(){
        $query= "UPDATE opening_product SET quantity =?,buy_price=? WHERE id='$this->id'";

        $STH = $this->DBH->prepare($query);

        $STH->bindParam(1,$this->quantity);
        $STH->bindParam(2,$this->buy_price);

        $result=$STH->execute();
        if($result){

            Message::setMessage("Success! Data successfully updated!");
        }
        else{
            Message::setMessage("Failed! data has not be updated!");
        }
    }
    public function showOpeningProduct(){
        $sql = "SELECT product.prod_name, product_cat.cat_name, opening_product.id, opening_product.quantity,opening_product.buy_price FROM `opening_product`,product,product_cat WHERE product.prod_id=opening_product.prod_id AND product_cat.id=product.cat_id AND opening_product.admin_id='$this->admin_id'";
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetchAll();
    }
    public function showOneOpeningProduct(){
        $sql = "SELECT product.prod_name, product_cat.cat_name, opening_product.id,opening_product.prod_id, opening_product.quantity,opening_product.buy_price FROM `opening_product`,product,product_cat WHERE product.prod_id=opening_product.prod_id AND product_cat.id=product.cat_id AND opening_product.id=$this->id";
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetch();
    }
    public function delete(){
        $query = "DELETE FROM `opening_product` WHERE id=$this->id";
        $this->DBH->exec($query);
    }
}