<?php
/**
 * Created by PhpStorm.
 * User: James Bond
 * Date: 7/4/2017
 * Time: 9:45 PM
 */

namespace App\model;
if(!isset($_SESSION) )  session_start();
use App\database\Database;
use App\Utility\Utility;
use PDO;
use App\Message\Message;

class Customer extends Database
{
    public $id;
    public $date;
    public $customer_name;
    public $contact;
    public $status;
    public $admin_id;

    public function __construct(){
        parent::__construct();
    }

    public function prepareData($data){
        if (array_key_exists('id', $data)) {
            $this->id = $data['id'];
        }
        if (array_key_exists('customer_name', $data)) {
            $this->customer_name = $data['customer_name'];
        }
        if (array_key_exists('contact', $data)) {
            $this->contact = $data['contact'];
        }
        if (array_key_exists('admin_id', $data)) {
            $this->admin_id = $data['admin_id'];
        }

        return $this;

    }
    public function store(){
        date_default_timezone_set('Asia/Dhaka');
        $date = date('Y-m-d H:i:s');
        $this->date=$date;
        $query= "INSERT INTO `customer`(name,contact,date,admin_id) VALUES (?,?,?,?)";

        $STH = $this->DBH->prepare($query);

        $STH->bindParam(1,$this->customer_name);
        $STH->bindParam(2,$this->contact);
        $STH->bindParam(3,$this->date);
        $STH->bindParam(4,$this->admin_id);
        $STH->execute();
    }
    public function showall(){
        $sql = "SELECT * FROM `customer` WHERE status='0' AND admin_id='$this->admin_id' GROUP BY contact ORDER BY id DESC";
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetchAll();
    }

    public function showPaidBill(){
        $sql = "SELECT bill_master.*,customer.name FROM `bill_master`,customer WHERE bill_master.status='0' AND customer.id=bill_master.cus_id AND bill_master.cus_id='$this->id' ORDER BY bill_master.bill_no DESC";
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetchAll();
    }

    public function showUnpaidBill(){
        $sql = "SELECT bill_master.*,customer.name FROM `bill_master`,customer WHERE bill_master.status='1' AND customer.id=bill_master.cus_id AND bill_master.cus_id='$this->id' ORDER BY bill_master.bill_no DESC";
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetchAll();
    }
    public function showVendor(){
        $sql = "SELECT `vendor_name`,`contact` FROM `purchase_master` WHERE admin_id='$this->admin_id' GROUP BY `contact` ORDER BY date DESC";
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetchAll();
    }
    public function showPaidMRR(){
        $sql = "SELECT * FROM `purchase_master` WHERE status='0' AND admin_id='$this->admin_id' AND contact='$this->contact' ORDER BY mrr_no DESC";
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetchAll();
    }
    public function showUnpaidMRR(){
        $sql = "SELECT * FROM `purchase_master` WHERE status='1' AND admin_id='$this->admin_id' AND contact='$this->contact' ORDER BY mrr_no DESC";
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetchAll();
    }

}