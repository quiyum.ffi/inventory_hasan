<?php
/**
 * Created by PhpStorm.
 * User: James Bond
 * Date: 7/4/2017
 * Time: 9:45 PM
 */

namespace App\model;
if(!isset($_SESSION) )  session_start();
use App\database\Database;
use App\Utility\Utility;
use PDO;
use App\Message\Message;

class Bill_master extends Database
{
    public $bill_no;
    public $date;
    public $customer_id;
    public $total_amount;
    public $paid;
    public $due;
    public $status;
    public $admin_id;



    public function __construct(){
        parent::__construct();
    }

    public function prepareData($data){
        if (array_key_exists('bill_no', $data)) {
            $this->bill_no = $data['bill_no'];
        }
        if (array_key_exists('total_sum', $data)) {
            $this->total_amount = $data['total_sum'];
        }
        if (array_key_exists('paid', $data)) {
            $this->paid = $data['paid'];
        }
        if (array_key_exists('due', $data)) {
            $this->due = $data['due'];
        }
        if (array_key_exists('status', $data)) {
            $this->status = $data['status'];
        }
        if (array_key_exists('admin_id', $data)) {
            $this->admin_id = $data['admin_id'];
        }

        return $this;

    }
    public function store(){
        date_default_timezone_set('Asia/Dhaka');
        $date = date('Y-m-d H:i:s');
        $this->date=$date;
        $select_master_id="SELECT id FROM `customer` order by id DESC Limit 1";
        $STH = $this->DBH->query($select_master_id);

        $STH->setFetchMode(PDO::FETCH_ASSOC);
        $STH->execute();
        $row=$STH->fetch();
        $this->customer_id= $row['id'];
        $query= "INSERT INTO `bill_master`(date,cus_id,total_amount,paid,due,status,admin_id) VALUES (?,?,?,?,?,?,?)";

        $STH = $this->DBH->prepare($query);

        $STH->bindParam(1,$this->date);
        $STH->bindParam(2,$this->customer_id);
        $STH->bindParam(3,$this->total_amount);
        $STH->bindParam(4,$this->paid);
        $STH->bindParam(5,$this->due);
        $STH->bindParam(6,$this->status);
        $STH->bindParam(7,$this->admin_id);

        $STH->execute();

    }
    public function showPaidList(){
        $sql = "SELECT bill_master.*,customer.name FROM `bill_master`,customer WHERE bill_master.status='0' AND customer.id=bill_master.cus_id AND bill_master.admin_id='$this->admin_id'  ORDER BY bill_master.bill_no DESC";
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetchAll();
    }
    public function showUnpaidList(){
        $sql = "SELECT bill_master.*,customer.name FROM `bill_master`,customer WHERE bill_master.status='1' AND customer.id=bill_master.cus_id AND bill_master.admin_id='$this->admin_id' ORDER BY bill_master.bill_no DESC";
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetchAll();
    }
    public function showDetails(){
        $sql = "SELECT bill_master.*,customer.name,customer.contact FROM `bill_master`,customer WHERE bill_no='$this->bill_no' AND customer.id=bill_master.cus_id";
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetch();
    }
    public function newPaid(){
        $query= "UPDATE bill_master SET paid =?,due=?,status=? WHERE bill_no=$this->bill_no";

        $STH = $this->DBH->prepare($query);

        $STH->bindParam(1,$this->paid);
        $STH->bindParam(2,$this->due);
        $STH->bindParam(3,$this->status);

        $result=$STH->execute();
        if($result){

            Message::setMessage("Success! Amount has been paid!");
        }
        else{
            Message::setMessage("Failed! data has not be updated!");
        }
    }

}