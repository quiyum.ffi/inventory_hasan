<?php
/**
 * Created by PhpStorm.
 * User: James Bond
 * Date: 7/29/2017
 * Time: 9:40 PM
 */

namespace App\model;
if(!isset($_SESSION) )  session_start();
use App\database\Database;
use App\Utility\Utility;
use PDO;
use App\Message\Message;

class Temp extends Database
{
    public $id;
    public $customer_name;
    public $contact;
    public $prod_id;
    public $quantity;
    public $price;
    public $total_price;
    public $admin_id;

    public function __construct(){
        parent::__construct();
    }

    public function prepareData($data){
        if (array_key_exists('id', $data)) {
            $this->id = $data['id'];
        }
        if (array_key_exists('customer_name', $data)) {
            $this->customer_name = $data['customer_name'];
        }
        if (array_key_exists('contact', $data)) {
            $this->contact = $data['contact'];
        }
        if (array_key_exists('product_id', $data)) {
            $this->prod_id = $data['product_id'];
        }
        if (array_key_exists('quantity', $data)) {
            $this->quantity = $data['quantity'];
        }
        if (array_key_exists('price', $data)) {
            $this->price = $data['price'];
        }
        if (array_key_exists('admin_id', $data)) {
            $this->admin_id = $data['admin_id'];
        }


        return $this;

    }
    public function store(){
        $this->total_price=(($this->quantity)*($this->price));
        $query= "INSERT INTO `temp`(p_id,quantity,prod_price,total,customer_name,contact,admin_id) VALUES (?,?,?,?,?,?,?)";

        $STH = $this->DBH->prepare($query);

        $STH->bindParam(1,$this->prod_id);
        $STH->bindParam(2,$this->quantity);
        $STH->bindParam(3,$this->price);
        $STH->bindParam(4,$this->total_price);
        $STH->bindParam(5,$this->customer_name);
        $STH->bindParam(6,$this->contact);
        $STH->bindParam(7,$this->admin_id);


        $STH->execute();
    }
    public function showData(){
        $sql = "SELECT temp.id,temp.p_id,temp.quantity,unit_lookup.unit,temp.prod_price,temp.total,product.prod_name FROM `temp`,product,unit_lookup WHERE product.prod_id=temp.p_id AND product.unit_id=unit_lookup.id";
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetchAll();
    }
    public function showCustomer(){
        $sql = "SELECT admin_id,customer_name,contact FROM temp GROUP BY admin_id";
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetch();
    }
    public function is_exist(){

        $query="SELECT * FROM `temp` WHERE admin_id='$this->admin_id'";
        $STH=$this->DBH->query($query);

        $STH->setFetchMode(PDO::FETCH_OBJ);
        $STH->fetchAll();

        $count = $STH->rowCount();

        if ($count > 0) {
            return TRUE;
        } else {
            return FALSE;
        }
    }
    public function total_price(){
        $sql = "SELECT sum(`total`) as total_sum FROM `temp` WHERE admin_id='$this->admin_id'";
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetch();
    }
    public function delete(){
        $query = "DELETE FROM `temp` WHERE admin_id='$this->admin_id'";
        $this->DBH->exec($query);
    }
    public function deleteOne(){
        $query = "DELETE FROM `temp` WHERE id='$this->id'";
        $this->DBH->exec($query);
    }
}