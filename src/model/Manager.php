<?php
/**
 * Created by PhpStorm.
 * User: James Bond
 * Date: 8/7/2017
 * Time: 1:59 AM
 */

namespace App\model;
if(!isset($_SESSION) )  session_start();
use App\database\Database;
use App\Utility\Utility;
use PDO;
use App\Message\Message;

class Manager extends Database
{
    public $id;
    public $date;
    public $manager_name;
    public $manager_contact;
    public $picture;
    public $password;
    public $status;
    public $user_name;
    public $u_name;
    public $admin_id;


    public function __construct(){
        parent::__construct();
    }

    public function prepareData($data){
        if (array_key_exists('id', $data)) {
            $this->id = $data['id'];
        }
        if (array_key_exists('manager_name', $data)) {
            $this->manager_name = $data['manager_name'];
        }
        if (array_key_exists('manager_contact', $data)) {
            $this->manager_contact = $data['manager_contact'];
        }
        if (array_key_exists('user_name', $data)) {
            $this->user_name = $data['user_name'];
        }
        if (array_key_exists('u_name', $data)) {
            $this->u_name = $data['u_name'];
        }

        if (array_key_exists('picture_name', $data)) {
            $this->picture = $data['picture_name'];
        }
        if (array_key_exists('password', $data)) {
            $this->password = md5($data['password']);
        }
        if (array_key_exists('admin_id', $data)) {
            $this->admin_id = $data['admin_id'];
        }
        

        return $this;

    }
    public function store(){
        date_default_timezone_set('Asia/Dhaka');
        $date = date('Y-m-d H:i:s');
        $this->date=$date;
        $query= "INSERT INTO `manager`(name,contact,user_name,password,assign_date,admin_id) VALUES (?,?,?,?,?,?)";

        $STH = $this->DBH->prepare($query);

        $STH->bindParam(1,$this->manager_name);
        $STH->bindParam(2,$this->manager_contact);
        $STH->bindParam(3,$this->user_name);
        $STH->bindParam(4,$this->password);
        $STH->bindParam(5,$this->date);
        $STH->bindParam(6,$this->admin_id);
        $STH->execute();
    }
    public function loginCheck(){
        $query = "SELECT * FROM `manager` WHERE `user_name`='$this->u_name' AND `password`='$this->password' AND status='1'";
        $STH=$this->DBH->query($query);
        $STH->setFetchMode(PDO::FETCH_ASSOC);
        $STH->fetchAll();


        $count = $STH->rowCount();
        if ($count > 0) {

            return TRUE;
        } else {
            return FALSE;
        }
    }
    public function changePic(){
        $query= "UPDATE manager SET picture =? WHERE id=$this->id";
        $STH = $this->DBH->prepare($query);

        $STH->bindParam(1,$this->picture);
        
        $STH->execute();
        
    }
    public function changeAdminPic(){
        $query= "UPDATE registration_info SET picture =? WHERE id=$this->id";
        $STH = $this->DBH->prepare($query);

        $STH->bindParam(1,$this->picture);

        $STH->execute();

    }
    public function changeAdminPass(){
        $query= "UPDATE registration_info SET password =? WHERE id=$this->id";
        $STH = $this->DBH->prepare($query);

        $STH->bindParam(1,$this->password);

        $STH->execute();

    }
    public function changeManagerPass(){
        $query= "UPDATE manager SET password =? WHERE id=$this->id";
        $STH = $this->DBH->prepare($query);

        $STH->bindParam(1,$this->password);

        $STH->execute();

    }


    public function deleteManager(){
        $this->status='0';
        $query= "UPDATE manager SET status =? WHERE id=$this->id";
        $STH = $this->DBH->prepare($query);

        $STH->bindParam(1,$this->status);
        
        $result=$STH->execute();
        if($result){

            Message::setMessage("Success! Manager activities successfully stoped!");
        }
        else{
            Message::setMessage("Failed! data has not be updated!");
        }
    }
    public function activeManager(){
        $this->status='1';
        $query= "UPDATE manager SET status =? WHERE id=$this->id";
        $STH = $this->DBH->prepare($query);

        $STH->bindParam(1,$this->status);

        $result=$STH->execute();
        if($result){

            Message::setMessage("Success! Manager activities successfully re-active!");
        }
        else{
            Message::setMessage("Failed! data has not be updated!");
        }
    }


    public function is_exist_user(){

        $query="SELECT * FROM `manager` WHERE `user_name`='$this->user_name'";
        $STH=$this->DBH->query($query);

        $STH->setFetchMode(PDO::FETCH_OBJ);
        $STH->fetchAll();

        $count = $STH->rowCount();

        if ($count > 0) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    public function showall(){
        $sql = "SELECT * FROM `manager` WHERE admin_id='$this->admin_id' AND status='1' ORDER BY id DESC";
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetchAll();
    }

    public function showdisabled(){
        $sql = "SELECT * FROM `manager` WHERE admin_id='$this->admin_id' AND status='0' ORDER BY id DESC";
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetchAll();
    }
    public function showOne(){
        $sql = "SELECT * FROM `manager` WHERE  admin_id='$this->admin_id' AND id='$this->id'";
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetch();
    }
    public function showOneUser(){
        $sql = "SELECT * FROM `registration_info` WHERE `user_name`='$this->u_name'";
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetch();
    }
    public function showOneManager(){
        $sql = "SELECT * FROM `manager` WHERE `user_name`='$this->u_name' AND admin_id='$this->admin_id' ";
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetch();
    }
    public function adminId(){
        $sql = "SELECT admin_id FROM `manager` WHERE `user_name`='$this->u_name' AND `password`='$this->password' AND status='1'";
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetch();

    }

}