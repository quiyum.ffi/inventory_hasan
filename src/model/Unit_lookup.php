<?php
/**
 * Created by PhpStorm.
 * User: James Bond
 * Date: 7/4/2017
 * Time: 9:47 PM
 */

namespace App\model;
if(!isset($_SESSION) )  session_start();
use App\database\Database;
use App\Utility\Utility;
use PDO;
use App\Message\Message;

class Unit_lookup extends Database
{
    public $id;
    public $unit;
    public $admin_id;

    public function __construct(){
        parent::__construct();
    }

    public function prepareData($data){
        if (array_key_exists('id', $data)) {
            $this->id = $data['id'];
        }
        if (array_key_exists('unit', $data)) {
            $this->unit = $data['unit'];
        }
        if (array_key_exists('admin_id', $data)) {
            $this->admin_id = $data['admin_id'];
        }


        return $this;

    }
    public function store(){
        $query= "INSERT INTO `unit_lookup`(admin_id,unit) VALUES (?,?)";

        $STH = $this->DBH->prepare($query);
        $STH->bindParam(1,$this->admin_id);
        $STH->bindParam(2,$this->unit);
        

        $result = $STH->execute();
        if($result){

            Message::setMessage("Success! Unit has been Added");
        }
        else{
            Message::setMessage("Failed! data has not be inserted!");
        }
    }
    public function showUnit(){
        $sql = "SELECT * FROM unit_lookup WHERE admin_id='$this->admin_id'";
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetchAll();
    }

    public function showOneUnit(){
        $sql = "SELECT * FROM unit_lookup WHERE id=$this->id";
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetch();
    }
    public function update(){
        $query= "UPDATE unit_lookup SET unit =? WHERE id=$this->id";

        $STH = $this->DBH->prepare($query);

        $STH->bindParam(1,$this->unit);

        $result=$STH->execute();
        if($result){

            Message::setMessage("Success! Data successfully updated!");
        }
        else{
            Message::setMessage("Failed! data has not be updated!");
        }
    }
}