<?php
/**
 * Created by PhpStorm.
 * User: James Bond
 * Date: 7/4/2017
 * Time: 9:45 PM
 */

namespace App\model;
if(!isset($_SESSION) )  session_start();
use App\database\Database;
use App\Utility\Utility;
use PDO;
use App\Message\Message;

class Bill_details extends Database
{
    public $id;
    public $bill_no;
    public $prod_id;
    public $quantity;
    public $prod_price;
    public $total;
    public $date;
    public $paid;
    public $bill_master;
    public $new_paid;
    public $total_pay;
    public $from_date;
    public $to_date;
    public $admin_id;
    public $current_date;

    public function __construct(){
        parent::__construct();
    }

    public function prepareData($data){
        if (array_key_exists('id', $data)) {
            $this->id = $data['id'];
        }
        if (array_key_exists('product_id', $data)) {
            $this->prod_id = $data['product_id'];
        }
        if (array_key_exists('p_quantity', $data)) {
            $this->quantity = $data['p_quantity'];
        }
        if (array_key_exists('p_price', $data)) {
            $this->prod_price = $data['p_price'];
        }
        if (array_key_exists('total_price', $data)) {
            $this->total = $data['total_price'];
        }
        if (array_key_exists('paid', $data)) {
            $this->paid = $data['paid'];
        }
        if (array_key_exists('bill_no', $data)) {
            $this->bill_master = $data['bill_no'];
        }
        if (array_key_exists('new_paid', $data)) {
            $this->new_paid = $data['new_paid'];
        }
        if (array_key_exists('total_paid_details', $data)) {
            $this->total_pay = $data['total_paid_details'];
        }
        if (array_key_exists('from_date', $data)) {
            $this->from_date = $data['from_date'];
        }
        if (array_key_exists('to_date', $data)) {
            $this->to_date = $data['to_date'];
        }
        if (array_key_exists('admin_id', $data)) {
            $this->admin_id = $data['admin_id'];
        }
        if (array_key_exists('current_date', $data)) {
            $this->current_date = $data['current_date'];
        }

        return $this;

    }
    public function store(){
        $select_master_id="SELECT bill_no FROM `bill_master` order by bill_no DESC Limit 1";
        $STH = $this->DBH->query($select_master_id);

        $STH->setFetchMode(PDO::FETCH_ASSOC);
        $STH->execute();
        $row=$STH->fetch();
        $this->bill_no= $row['bill_no'];

        $pr_id=explode(",",$this->prod_id);
        $quan=explode(",",$this->quantity);
        $price=explode(",",$this->prod_price);
        $total=explode(",",$this->total);
        $count=0;
        foreach($pr_id as $pr){
            $query= "INSERT INTO bill_details (bill_no,prod_id,quantity,prod_price,total,admin_id) VALUES (?,?,?,?,?,?)";
            $STH = $this->DBH->prepare($query);
            $STH->bindParam(1,$this->bill_no);
            $STH->bindParam(2,$pr_id[$count]);
            $STH->bindParam(3,$quan[$count]);
            $STH->bindParam(4,$price[$count]);
            $STH->bindParam(5,$total[$count]);
            $STH->bindParam(6,$this->admin_id);
            $STH->execute();
            $count++;
        }

    }
    public function storeBill(){
        $select_master_id="SELECT bill_no FROM `bill_master` order by bill_no DESC Limit 1";
        $STH = $this->DBH->query($select_master_id);

        $STH->setFetchMode(PDO::FETCH_ASSOC);
        $STH->execute();
        $row=$STH->fetch();
        $this->bill_no= $row['bill_no'];

        date_default_timezone_set('Asia/Dhaka');
        $date = date('Y-m-d H:i:s');
        $this->date=$date;
        $query= "INSERT INTO `sale_bill`(bill_master_id,payment,total_payment,date) VALUES (?,?,?,?)";

        $STH = $this->DBH->prepare($query);

        $STH->bindParam(1,$this->bill_no);
        $STH->bindParam(2,$this->paid);
        $STH->bindParam(3,$this->paid);
        $STH->bindParam(4,$this->date);
        $STH->execute();
    }
    public function showListDetails(){
        $sql = "SELECT product.prod_name, bill_details.* FROM `bill_details`,product WHERE product.prod_id=bill_details.prod_id AND bill_details.bill_no=$this->bill_master";
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetchAll();
    }
    public function showBillDetails(){
        $sql = "SELECT * FROM `sale_bill` WHERE bill_master_id=$this->bill_master ORDER BY id DESC";
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetchAll();
    }
    public function newPaidBill(){
        date_default_timezone_set('Asia/Dhaka');
        $date = date('Y-m-d H:i:s');
        $this->date=$date;
        $query= "INSERT INTO `sale_bill`(bill_master_id,payment,total_payment,date) VALUES (?,?,?,?)";

        $STH = $this->DBH->prepare($query);

        $STH->bindParam(1,$this->bill_master);
        $STH->bindParam(2,$this->new_paid);
        $STH->bindParam(3,$this->total_pay);
        $STH->bindParam(4,$this->date);
        $STH->execute();
    }
    public function showall(){
        $sql = "SELECT product.prod_name,unit_lookup.unit,bill_master.date,bill_details.* FROM `bill_details`,product,bill_master,unit_lookup WHERE product.prod_id=bill_details.prod_id AND bill_master.bill_no=bill_details.bill_no AND product.unit_id=unit_lookup.id AND bill_master.admin_id='$this->admin_id' ORDER BY bill_master.date DESC";
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetchAll();
    }
    public function showSelectedDate(){
        $sql = "SELECT product.prod_name,unit_lookup.unit,bill_master.date,bill_details.* FROM `bill_details`,product,bill_master,unit_lookup WHERE product.prod_id=bill_details.prod_id AND bill_master.bill_no=bill_details.bill_no AND product.unit_id=unit_lookup.id AND bill_master.admin_id='$this->admin_id' AND DATE(bill_master.date) BETWEEN '$this->from_date' AND '$this->to_date' ORDER BY bill_master.date DESC";
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetchAll();
    }
    public function showSelectedDate2(){
        $sql = "SELECT product.prod_name,unit_lookup.unit,bill_master.date,bill_details.* FROM `bill_details`,product,bill_master,unit_lookup WHERE product.prod_id=bill_details.prod_id AND bill_master.bill_no=bill_details.bill_no AND product.unit_id=unit_lookup.id AND bill_master.admin_id='$this->admin_id' AND DATE(bill_master.date)='$this->current_date' ORDER BY bill_master.date DESC";
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetchAll();
    }

}