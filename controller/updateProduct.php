<?php
require_once ("../vendor/autoload.php");
use App\model\Product;
use App\Message\Message;
use App\Utility\Utility;
$object=new Product();
    $object->prepareData($_POST);
    $status=$object->is_exist();
if($status){
    Message::setMessage("This product already exist");
    return Utility::redirect($_SERVER['HTTP_REFERER']);
}
else{
    $object->update();
    return Utility::redirect($_SERVER['HTTP_REFERER']);
}



