<?php
require_once ("../vendor/autoload.php");
use App\model\Opening_product;
use App\Utility\Utility;
$object=new Opening_product();
$object->prepareData($_GET);
$object->update();
return Utility::redirect($_SERVER['HTTP_REFERER']);