<?php
require_once ("../vendor/autoload.php");
use App\model\Purchase_master;
use App\model\Purchase_details;
use App\model\Temp2;
use App\Message\Message;
use App\Utility\Utility;
$_POST['due']=$_POST['total_sum']-$_POST['paid'];
if($_POST['due']=='0'){
    $_POST['status']=0;
}
else
{
    $_POST['status']=1;
}
if($_POST['total_sum']<$_POST['paid']){
    Message::setMessage("Failed! payment amount can't bigger than total amount");
    Utility::redirect('../views/purchaseProduct.php');
}
else{
    $object=new Purchase_master();
    $object->prepareData($_POST);
    $object->store();
    $_POST['product_id']=implode(",",$_POST['p_id']);
    $_POST['p_quantity']=implode(",",$_POST['quantity']);
    $_POST['p_price']=implode(",",$_POST['price']);
    $_POST['total_price']=implode(",",$_POST['total']);
    $detailsObj=new Purchase_details();
    $detailsObj->prepareData($_POST);
    $detailsObj->store();
    $detailsObj->storeBill();
    $temp=new Temp2();
    $temp->prepareData($_POST);
    $temp->delete();
    Message::setMessage("Success! Purchase has been successfully");
    Utility::redirect('../views/purchaseList.php');
}

