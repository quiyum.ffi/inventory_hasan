<?php
require_once("../../vendor/autoload.php");
use App\model\Manager;
use App\Utility\Utility;
use App\Message\Message;
if(isset($_FILES['picture']['name']))
{
    $picName=time().$_FILES['picture']['name'];
    $tmp_name=$_FILES['picture']['tmp_name'];

    move_uploaded_file($tmp_name,'../../resources/manager_photos/'.$picName);
    $_POST['picture_name']=$picName;
}
$object= new Manager();
$object->prepareData($_POST);
$object->changePic();
Message::setMessage("Success! Picture has been changed successfully");
return Utility::redirect($_SERVER['HTTP_REFERER']);

?>