<?php
require_once ("../../vendor/autoload.php");
use App\model\Temp2;
use App\Message\Message;
use App\Utility\Utility;
$object=new Temp2();
if($_POST['product_id']=='reject')
{
    Message::setMessage("Please Select a product");
    return Utility::redirect($_SERVER['HTTP_REFERER']);
}
else{
    $object->prepareData($_POST);
    $object->store();
    return Utility::redirect($_SERVER['HTTP_REFERER']);
}