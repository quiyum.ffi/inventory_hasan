<?php
require_once("../../vendor/autoload.php");
use App\model\Manager;
use App\Utility\Utility;
use App\Message\Message;
$object= new Manager();
if($_POST['password']==$_POST['c_password']){
    $object->prepareData($_POST);
    $object->changeManagerPass();
    Message::setMessage("Password successfully changed!");
    return Utility::redirect($_SERVER['HTTP_REFERER']);
}
else{
    Message::setMessage(" Failed! Confirm password doesn't match!");
    return Utility::redirect($_SERVER['HTTP_REFERER']);
}


?>