<?php
require_once ("../../vendor/autoload.php");
use App\model\Registration_info;
use App\Utility\Utility;
use App\Message\Message;
$object=new Registration_info();
$object->prepareData($_POST);
$emailStatus=$object->is_exist_email();
$userStatus=$object->is_exist_user();

if($emailStatus){
    Message::setMessage("This email has been already taken!");
    return Utility::redirect($_SERVER['HTTP_REFERER']);
}
else if($userStatus){
    Message::setMessage("This username has been already taken!");
    return Utility::redirect($_SERVER['HTTP_REFERER']);
}
else{
    if($_POST['password']==$_POST['c_password']){
        $object->store();
    }
    else{
        Message::setMessage("Registration Failed! Confirm password doesn't match!");
        return Utility::redirect($_SERVER['HTTP_REFERER']);
    }
}

