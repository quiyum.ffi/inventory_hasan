<?php
require_once ('../../vendor/autoload.php');
use App\model\Purchase_master;
use App\model\Purchase_details;
use App\Utility\Utility;
use App\Message\Message;
$object=new Purchase_master();
$detailsObj=new Purchase_details();
$object->prepareData($_POST);
$allData=$object->showDetails();
if($_POST['new_paid']>$allData->due){
    Message::setMessage("Amount can't be bigger than due! Try Again");
    return Utility::redirect($_SERVER['HTTP_REFERER']);
}
else{
    $_POST['paid']=$allData->paid+$_POST['new_paid'];
    $_POST['due']=$allData->total_amount-$_POST['paid'];
    if($_POST['due']=='0'){
        $_POST['status']=0;
    }
    else
    {
        $_POST['status']=1;
    }
    $object->prepareData($_POST);
    $object->newPaid();
    $detailsObj->prepareData($_POST);
    $detailsObj->newPaidBill();

    return Utility::redirect($_SERVER['HTTP_REFERER']);
}

