<?php
require_once("../vendor/autoload.php");
use App\model\Manager;
use App\Utility\Utility;
use App\Message\Message;
$object= new Manager();
$object->prepareData($_GET);
$object->deleteManager();
Utility::redirect('../views/managerDetails.php');
?>