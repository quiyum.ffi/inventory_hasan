<?php
require_once ("../vendor/autoload.php");
use App\model\Temp;
use App\model\Product;
use App\Message\Message;
use App\Utility\Utility;
$stock=new Product();
$stock->prepareData($_POST);
$stockData=$stock->stockList();
$available=$stockData->bal_qty." ".$stockData->unit;
if($stockData->bal_qty>=$_POST['quantity']){
    $object=new Temp();
    if($_POST['product_id']=='reject')
    {
        Message::setMessage("Please Select a product");
        return Utility::redirect($_SERVER['HTTP_REFERER']);
    }
    else{
        $object->prepareData($_POST);
        $object->store();
        Utility::redirect('../views/saleProduct.php');
    }
}
else{
    Message::setMessage("Reject! This product available is $available");
    return Utility::redirect($_SERVER['HTTP_REFERER']);
}


