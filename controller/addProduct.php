<?php
require_once ("../vendor/autoload.php");
use App\model\Product;
use App\Message\Message;
use App\Utility\Utility;
$object=new Product();
if($_POST['cat_id']=='reject' || $_POST['unit_id']=='reject')
{ 
    Message::setMessage("Please Select both of the category and unit");
    return Utility::redirect($_SERVER['HTTP_REFERER']);
}
else{
    $object->prepareData($_POST);
    $status=$object->is_exist();
    if($status){
        Message::setMessage("This product already stored");
        return Utility::redirect($_SERVER['HTTP_REFERER']);
    }
    else{
        $object->store();
        return Utility::redirect($_SERVER['HTTP_REFERER']); 
    }
}

