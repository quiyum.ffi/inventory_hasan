<?php
require_once("../vendor/autoload.php");
use App\model\Super_admin;
use App\Utility\Utility;
use App\Message\Message;
$object= new Super_admin();
if($_POST['password']==$_POST['c_password']){
    $object->prepareData($_POST);
    $object->changeSadminPass();
    Message::setMessage("Password successfully changed!");
    return Utility::redirect($_SERVER['HTTP_REFERER']);
}
else{
    Message::setMessage(" Failed! Confirm password doesn't match!");
    return Utility::redirect($_SERVER['HTTP_REFERER']);
}


?>