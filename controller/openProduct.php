<?php
require_once ("../vendor/autoload.php");
use App\model\Opening_product;
use App\Message\Message;
use App\Utility\Utility;
$object=new Opening_product();
if($_POST['product_name']=='reject')
{
    Message::setMessage("Please Select a product");
    return Utility::redirect($_SERVER['HTTP_REFERER']);
}
else{
    $object->prepareData($_POST);
    $object->store();
    return Utility::redirect($_SERVER['HTTP_REFERER']);
}

