<?php
require_once ("../vendor/autoload.php");
use App\model\Product_cat;
use App\Utility\Utility;
use App\Message\Message;
$object=new Product_cat();
$object->prepareData($_GET);
$object->delete();
Message::setMessage("Success! Category has been delete!");
Utility::redirect('../views/productCat&Unit.php');