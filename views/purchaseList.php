<?php
session_start();
require_once ("../vendor/autoload.php");
require_once ("templateLayout/information.php");
use App\model\Registration_info;
use App\Utility\Utility;
use App\Message\Message;
if($_SESSION['role_status']==0){
    $auth= new Registration_info();
    $status = $auth->prepareData($_SESSION)->logged_in();

    if(!$status) {
        Utility::redirect('login.php');
        Message::setMessage("Please LogIn first");
        return;
    }
}
else {
    Message::setMessage("Please LogIn first");
    Utility::redirect('login.php');
}
use App\model\Purchase_master;
$masterObj=new Purchase_master();
$masterObj->prepareData($_SESSION);
$paidList=$masterObj->showPaidList();
$unpaidList=$masterObj->showUnpaidList();
?>
<!DOCTYPE HTML>
<html>
<head>
    <title><?php echo $title?></title>
    <?php require_once ("templateLayout/templateCss.php");?>
</head>
<body>
<div class="page-container">
    <div class="left-content">
        <div class="mother-grid-inner">
            <?php require_once ("templateLayout/header.php")?>
            <div class="inner-block">
                <div class="row">
                    <div class="col-md-12">
                        <div class="pro-head">
                            <h2 style="text-align: center">Full Paid List</h2>
                        </div>
                        <?php
                        if(isset($_SESSION) && !empty($_SESSION['message'])) {

                            $msg = Message::getMessage();

                            echo "<p class='help-block' style='color: #0c5577;text-align: center'>$msg</p>";
                        }

                        ?>
                        <div >
                            <table id="example" class="table table-bordered table-striped">
                                <thead>
                                <tr>
                                    <th>Serial</th>
                                    <th>MRR NO</th>
                                    <th>Vendor Name</th>
                                    <th>Date</th>
                                    <th>Time</th>
                                    <th>Total Amount</th>
                                    <th>Paid Amount</th>
                                    <th>Due</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tfoot>
                                <tr>
                                    <th>Serial</th>
                                    <th>MRR NO</th>
                                    <th>Vendor Name</th>
                                    <th>Date</th>
                                    <th>Time</th>
                                    <th>Total Amount</th>
                                    <th>Paid Amount</th>
                                    <th>Due</th>
                                    <th>Action</th>
                                </tr>
                                </tfoot>
                                <tbody>
                                <?php
                                $serial= 1;
                                foreach ($paidList as $oneData){
                                    $date = date("d/m/Y", strtotime("$oneData->date"));
                                    $time = date("h:i A", strtotime("$oneData->date"));
                                    ?>
                                    <tr>
                                        <td><?php echo $serial?></td>
                                        <td><?php echo $oneData->mrr_no?></td>
                                        <td><?php echo $oneData->vendor_name?></td>
                                        <td><?php echo $date?></td>
                                        <td><?php echo $time?></td>
                                        <td><?php echo $oneData->total_amount?></td>
                                        <td><?php echo $oneData->paid?></td>
                                        <td><?php echo $oneData->due?></td>
                                        <td style="text-align: center"><a href='purchaseDetails.php?mrr_no=<?php echo $oneData->mrr_no?>' class='btn btn-info'><i class='fa fa-external-link-square ' aria-hidden='true'></i></a>
                                        </td>
                                    </tr>
                                    <?php
                                    $serial++;
                                }
                                ?>

                                </tbody>
                            </table>




                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="pro-head">
                            <h2 style="text-align: center">Unpaid List</h2>
                        </div>
                        <div >
                            <table id="example3" class="table table-bordered table-striped">
                                <thead>
                                <tr>
                                    <th>Serial</th>
                                    <th>MRR NO</th>
                                    <th>Vendor Name</th>
                                    <th>Date</th>
                                    <th>Time</th>
                                    <th>Total Amount</th>
                                    <th>Paid Amount</th>
                                    <th>Due</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tfoot>
                                <tr>
                                    <th>Serial</th>
                                    <th>MRR NO</th>
                                    <th>Vendor Name</th>
                                    <th>Date</th>
                                    <th>Time</th>
                                    <th>Total Amount</th>
                                    <th>Paid Amount</th>
                                    <th>Due</th>
                                    <th>Action</th>
                                </tr>
                                </tfoot>
                                <tbody>
                                <?php
                                $serial= 1;
                                foreach ($unpaidList as $oneData){
                                    $date = date("d/m/Y", strtotime("$oneData->date"));
                                    $time = date("h:i A", strtotime("$oneData->date"));
                                    ?>
                                    <tr>
                                        <td><?php echo $serial?></td>
                                        <td><?php echo $oneData->mrr_no?></td>
                                        <td><?php echo $oneData->vendor_name?></td>
                                        <td><?php echo $date?></td>
                                        <td><?php echo $time?></td>
                                        <td><?php echo $oneData->total_amount?></td>
                                        <td><?php echo $oneData->paid?></td>
                                        <td><?php echo $oneData->due?></td>
                                        <td style="text-align: center"><a href='purchaseDetails.php?mrr_no=<?php echo $oneData->mrr_no?>' class='btn btn-info'><i class='fa fa-external-link-square ' aria-hidden='true'></i></a>
                                        </td>
                                    </tr>
                                    <?php
                                    $serial++;
                                }
                                ?>

                                </tbody>
                            </table>




                        </div>
                    </div>
                </div>


            </div>
            <!--inner block end here-->
            <?php require_once ("templateLayout/footer.php");?>
        </div>
    </div>
    <!--slider menu-->
    <?php require_once ("templateLayout/navigation.php");?>
    <div class="clearfix"> </div>
</div>
<!--slide bar menu end here-->
<?php require_once ("templateLayout/templateScript.php")?>
</body>
</html>




