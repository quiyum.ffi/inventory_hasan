<?php
session_start();
require_once ("../vendor/autoload.php");
require_once ("templateLayout/information.php");
use App\Message\Message;
?>
<!DOCTYPE HTML>
<html>
<head>
    <title><?php echo $title?></title>
    <?php require_once ("templateLayout/templateCss.php");?>
</head>
<body>
<div class="login-page">
    <div class="login-main">
        <div class="login-head">
            <h1>Login</h1>
        </div>
        <div class="login-block">
            <?php




            if(isset($_SESSION) && !empty($_SESSION['message'])) {

                $msg = Message::getMessage();

                echo "<p class='help-block' style='color: #0c5577;text-align: center'>$msg</p>";
            }

            ?>
            <form action="../controller/login.php" method="post">
                <select name="login_status">
                    <option value="reject">-Select Panel-</option>
                    <option value="0"> Super Admin</option>
                    <option value="1"> Admin</option>
                    <option value="2">Manager</option>
                </select>
                <input type="text" name="u_name" placeholder="Username" required="">
                <input type="password" name="password" class="lock" placeholder="Password">
                <div class="forgot-top-grids">

                    <div class="forgot">
                        <a href="#">Forgot password?</a>
                    </div>
                    <div class="clearfix"> </div>
                </div>
                <input type="submit" >
               
            </form>
            <div class="sign-down">
                <h4>Not Registered? <a href="sign_up.php"> Sign Up here.</a></h4>
            </div>
        </div>
    </div>
</div>
<!--inner block end here-->
<!--copy rights start here-->
<?php require_once ("templateLayout/footer.php");?>
<!--COPY rights end here-->

<?php require_once ("templateLayout/templateScript.php")?>
</body>
</html>




