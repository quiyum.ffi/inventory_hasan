<?php
session_start();
require_once ("../../vendor/autoload.php");
require_once ("../templateLayout/information.php");
use App\model\Registration_info;
use App\Utility\Utility;
use App\Message\Message;
if($_SESSION['role_status']==2){
    $auth= new Registration_info();
    $status = $auth->prepareData($_SESSION)->logged_in();

    if(!$status) {
        Utility::redirect('../login.php');
        Message::setMessage("Please LogIn first");
        return;
    }
}
else {
    Message::setMessage("Please LogIn first");
    Utility::redirect('../login.php');
}
use App\model\Super_admin;
$object=new Super_admin();
$object->prepareData($_SESSION);
$oneData=$object->showOneSadmin();
//var_dump($oneData);
?>
<!DOCTYPE HTML>
<html>
<head>
    <title><?php echo $title?></title>
    <?php require_once ("../templateLayout/templateCss.php");?>
</head>
<body>
<div class="page-container">
    <div class="left-content">
        <div class="mother-grid-inner">
            <?php require_once ("../templateLayout/superAdminHeader.php")?>
            <div class="inner-block">
                <div class="row" style="min-height: 600px">
                    <div class="col-md-6 ">
                        <div class="pro-head">
                            <h2 style="text-align: center">Super Admin Profile</h2>
                        </div>
                        <?php
                        if(isset($_SESSION) && !empty($_SESSION['message'])) {

                            $msg = Message::getMessage();

                            echo "<p class='help-block' style='color: #0c5577;text-align: center'>$msg</p>";
                        }

                        ?>
                        <div class="row">
                            <div class="col-md-6 col-md-offset-3">
                                <div class="col-md-10 col-md-offset-1">
                                    <div style="height: 200px;" class="thumbnail">
                                        <img src="../../resources/sadmin_photos/<?php echo $oneData->picture?>" class="img-responsive img-rounded">
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <h5 style="font-family: 'Maiandra GD';"><p style="width: 100px;float: left">Name</p>:       <?php echo $oneData->name?></h5>
                                    <h5 style="font-family: 'Maiandra GD';"><p style="width: 100px;float: left">User Name</p>:      <?php echo $oneData->username?></h5>
                                    <h5 style="font-family: 'Maiandra GD';"><p style="width: 100px;float: left">Contact</p>:      <?php echo $oneData->contact?></h5>
                                    <br>
                                    <a href="profile.php?pic_change=true" class="btn btn-primary" style="width: 100%">Change Picture</a><br><br>
                                    <a href="profile.php?pass_change=true" class="btn btn-info" style="width: 100%">Change Password</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="pro-head">
                            <h2 style="text-align: center">Super Admin Profile</h2>
                        </div>
                        <div style="height:500px; border: 2px #0c5577 solid">
                            <?php
                            if(isset($_GET['pic_change']) && !empty($_GET['pic_change'])){
                                ?>
                                <div class="login-block">
                                    <form action="../../controller/changeSadminPic.php" method="post" enctype="multipart/form-data">
                                        <input type="file" name="picture" required="">
                                        <input type="hidden" name="id" value="<?php echo $oneData->id?>">
                                        <input type="submit" value="Change Picture" class="btn btn-primary">
                                    </form>
                                </div>
                                <?php
                            }
                            else if(isset($_GET['pass_change']) && !empty($_GET['pass_change'])){
                                ?>
                                <div class="login-block">
                                    <form action="../../controller/changeSadminPass.php" method="post" enctype="multipart/form-data">
                                        <input type="password" name="password" placeholder="password (at least 6 characters)" minlength="6" required="">
                                        <input type="password" name="c_password" placeholder="Confirm password (at least 6 characters)" minlength="6" required="">
                                        <input type="hidden" name="id" value="<?php echo $oneData->id?>">
                                        <input type="submit" value="Change Password" class="btn btn-primary">
                                    </form>
                                </div>
                                <?php
                            }
                            else{
                                ?>
                                <p style="line-height: 500px;text-align: center;font-size: 30px; color: #0c5577">Inventory Management System</p>

                                <?php
                            }
                            ?>
                        </div>
                    </div>
                </div>
            </div>
            <!--inner block end here-->
            <?php require_once ("../templateLayout/footer.php");?>
        </div>
    </div>
    <!--slider menu-->
    <?php require_once ("../templateLayout/superAdminNavigation.php");?>
    <div class="clearfix"> </div>
</div>
<!--slide bar menu end here-->
<?php require_once ("../templateLayout/templateScript.php")?>
</body>
</html>




