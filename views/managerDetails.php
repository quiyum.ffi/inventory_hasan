<?php
session_start();
require_once ("../vendor/autoload.php");
require_once ("templateLayout/information.php");
use App\model\Registration_info;
use App\Utility\Utility;
use App\Message\Message;
if($_SESSION['role_status']==0){
    $auth= new Registration_info();
    $status = $auth->prepareData($_SESSION)->logged_in();

    if(!$status) {
        Utility::redirect('login.php');
        Message::setMessage("Please LogIn first");
        return;
    }
}
else {
    Message::setMessage("Please LogIn first");
    Utility::redirect('login.php');
}
use App\model\Manager;
$object=new Manager();
$object->prepareData($_SESSION);
$allData=$object->showall();
$disable=$object->showdisabled();

?>
<!DOCTYPE HTML>
<html>
<head>
    <title><?php echo $title?></title>
    <?php require_once ("templateLayout/templateCss.php");?>
</head>
<body>
<div class="page-container">
    <div class="left-content">
        <div class="mother-grid-inner">
            <?php require_once ("templateLayout/header.php")?>
            <div class="inner-block">
                <div class="row" style="min-height: 600px">
                    <div class="col-md-10 col-md-offset-1">
                        <div class="pro-head">
                            <h2 style="text-align: center">Manager Lookup</h2>
                            <hr>
                        </div>
                        <?php
                        if(isset($_SESSION) && !empty($_SESSION['message'])) {

                            $msg = Message::getMessage();

                            echo "<p class='help-block' style='color: #0c5577;text-align: center'>$msg</p>";
                        }

                        ?>
                        <div class="row">
                            <?php
                                foreach ($allData as $oneData){
                                    ?>
                                    <div class="col-md-2">
                                        <div>
                                            <div style="height: 150px;" class="thumbnail">
                                                <img src="../resources/manager_photos/<?php echo $oneData->picture?>" class="img-responsive img-rounded">
                                            </div>
                                            <a href="managerProfile.php?id=<?php echo $oneData->id?>"><h4 style="font-family: 'Maiandra GD';text-align: center"><?php echo $oneData->name?></h4></a>
                                            <br>
                                            <a href="managerProfile.php?id=<?php echo $oneData->id?>" class="btn btn-primary" style="width: 100%">View Profile</a>
                                        </div>

                                    </div>
                            <?php
                                }
                            ?>
                        </div>
                    </div>
                    <br><br><br>
                    <div class="col-md-10 col-md-offset-1">
                        <div class="pro-head">
                            <h2 style="text-align: center">Disabled Manager Lookup</h2>
                            <hr>
                        </div>
                        <div class="row">
                            <table id="example" class="table table-bordered table-striped" >
                                <thead>
                                <tr>
                                    <th>Serial</th>
                                    <th>Manager Name</th>
                                    <th>User Name</th>
                                    <th>Contact</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tfoot>
                                <tr>
                                    <th>Serial</th>
                                    <th>Manager Name</th>
                                    <th>User Name</th>
                                    <th>Contact</th>
                                    <th>Action</th>
                                </tr>
                                </tfoot>
                                <tbody>
                                <?php
                                $serial= 1;
                                foreach ($disable as $oneData){

                                    ?>
                                    <tr>
                                        <td><?php echo $serial?></td>
                                        <td><?php echo $oneData->name?></td>
                                        <td><?php echo $oneData->user_name?></td>
                                        <td><?php echo $oneData->contact?></td>
                                        <td style="text-align: center"><a href='managerProfile.php?id=<?php echo $oneData->id?>' class='btn btn-info'><i class='fa fa-external-link-square ' aria-hidden='true'></i></a>
                                        </td>
                                    </tr>
                                    <?php
                                    $serial++;
                                }
                                ?>

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>


            </div>
            <!--inner block end here-->
            <?php require_once ("templateLayout/footer.php");?>
        </div>
    </div>
    <!--slider menu-->
    <?php require_once ("templateLayout/navigation.php");?>
    <div class="clearfix"> </div>
</div>
<!--slide bar menu end here-->
<?php require_once ("templateLayout/templateScript.php")?>
</body>
</html>




