<div class="sidebar-menu">
    <div class="logo"> <a href="#" class="sidebar-icon"> <span class="fa fa-bars"></span> </a> <a href="#"> <span id="logo" ></span>
            <!--<img id="logo" src="" alt="Logo"/>-->
        </a> </div>
    <div class="menu">
        <ul id="menu" >
            <li id="menu-home" ><a href="index.php"><i class="fa fa-tachometer"></i><span>Dashboard</span></a></li>
            <li id="menu-home" ><a href="profile.php"><i class="fa fa-tachometer"></i><span>Profile</span></a></li>
            <li><a href="#"><i class="fa fa-cogs"></i><span>Components</span><span class="fa fa-angle-right" style="float: right"></span></a>
                <ul>
                    <li><a href="<?php base_url?>productCat&Unit.php">Add Category & Units</a></li>
                    <li><a href="<?php base_url?>addProduct.php">Add Products</a></li>
                </ul>
            </li>
            <li><a href="<?php base_url?>opening_product.php"><i class="fa fa-book nav_icon"></i><span>Opening Products</span></a></li>
            <li><a href="#"><i class="fa fa-cogs"></i><span>Purchase Products</span><span class="fa fa-angle-right" style="float: right"></span></a>
                <ul>
                    <li><a href="<?php base_url?>purchaseProduct.php">Purchase Products</a></li>
                    <li><a href="<?php base_url?>purchaseList.php">Purchase List</a></li>
                </ul>
            </li>
            <li><a href="#"><i class="fa fa-cogs"></i><span>Customer & Vendor</span><span class="fa fa-angle-right" style="float: right"></span></a>
                <ul>
                    <li><a href="<?php base_url?>customerDetails.php">Customer Details</a></li>
                    <li><a href="<?php base_url?>vendorDetails.php">Vendor Details</a></li>
                </ul>
            </li>
            <li><a href="#"><i class="fa fa-cogs"></i><span>Sale</span><span class="fa fa-angle-right" style="float: right"></span></a>
                <ul>
                    <li><a href="<?php base_url?>saleProduct.php">Sale Products</a></li>
                    <li><a href="<?php base_url?>saleList.php">Sale List</a></li>
                </ul>
            </li>
            <li><a href="#"><i class="fa fa-cogs"></i><span>Report</span><span class="fa fa-angle-right" style="float: right"></span></a>
                <ul>
                    <li><a href="<?php base_url?>stock.php">Stock</a></li>
                    <li><a href="<?php base_url?>purchaseReport.php">Purchase Report</a></li>
                    <li><a href="<?php base_url?>salesReport.php">Sale Report</a></li>
                </ul>
            </li>
            <li><a href="#"><i class="fa fa-cogs"></i><span>Manager</span><span class="fa fa-angle-right" style="float: right"></span></a>
                <ul>
                    <li><a href="<?php base_url?>assignManager.php">Add Manager</a></li>
                    <li><a href="<?php base_url?>managerDetails.php">Manager Lookup</a></li>
                </ul>
            </li>
        </ul>
    </div>
</div>