<?php
session_start();
require_once ("../../vendor/autoload.php");
require_once ("../templateLayout/information.php");
use App\model\Registration_info;
use App\Utility\Utility;
use App\Message\Message;
if($_SESSION['role_status']==1){
    $auth= new Registration_info();
    $status = $auth->prepareData($_SESSION)->logged_in();

    if(!$status) {
        Utility::redirect('../login.php');
        Message::setMessage("Please LogIn first");
        return;
    }
}
else {
    Message::setMessage("Please LogIn first");
    Utility::redirect('../login.php');
}
use App\model\Purchase_details;
$product=new Purchase_details();
if(isset($_POST) && !empty($_POST)){
    $from_date=$_POST['from_date'];
    $to_date=$_POST['to_date'];
    if($from_date<$to_date){
        $product->prepareData($_POST);
        $allDataSelected=$product->showSelectedDate();
    }



}
$allData=$product->showall();
?>
<!DOCTYPE HTML>
<html>
<head>
    <title><?php echo $title?></title>
    <?php require_once ("../templateLayout/templateCss.php");?>
</head>
<body>
<div class="page-container">
    <div class="left-content">
        <div class="mother-grid-inner">
            <?php require_once ("../templateLayout/managerHeader.php")?>
            <div class="inner-block">
                <div class="row" style="min-height: 600px">
                    <?php
                    if(isset($_SESSION) && !empty($_SESSION['message'])) {

                        $msg = Message::getMessage();

                        echo "<p class='help-block' style='color: #0c5577;text-align: center'>$msg</p>";
                    }
                    ?>

                    <div class="col-md-8 col-md-offset-2">
                        <div class="pro-head">
                            <h2 style="text-align: center">Purchase Report</h2>
                        </div>
                        <div >
                            <form action="purchaseReport.php" method="post">
                                <div class="row">
                                    <div class="">
                                        <div class="col-md-4">
                                            From
                                            <input type="date" name="from_date" class="form-control" required>
                                        </div>
                                        <div class="col-md-4 ">
                                            To
                                            <input type="date" name="to_date" class="form-control" required>
                                        </div>
                                        <div class="col-md-2 ">
                                            <br>
                                            <input type="submit" value="Search" class="btn btn-primary">
                                        </div>
                                    </div>
                                </div>
                            </form>
<br><br>
                            <hr>
                            <?php
                                if(isset($_POST) && !empty($_POST)){
                                    if(isset($_POST) && !empty($_POST)){
                                        $from_date=$_POST['from_date'];
                                        $to_date=$_POST['to_date'];
                                        if($from_date<$to_date){
                                            $date_from = date("d/m/Y", strtotime($from_date));
                                            $date_to = date("d/m/Y", strtotime($to_date));
                                            ?>
                                            <form action="printPurchase.php" method="post">
                                                <input type="hidden" name="from_date" value="<?php echo $from_date?>">
                                                <input type="hidden" name="to_date" value="<?php echo $to_date?>">
                                                <center><input type="submit" value="Print This List" class="btn btn-primary"></center>
                                            </form>
                                            <br>

                                            <h5 style="text-align: center">From: <?php echo $date_from?> To: <?php echo $date_to?></h5><br>
                                            <table id="example" class="table table-bordered table-striped">
                                                <thead>
                                                <tr>
                                                    <th>Serial</th>
                                                    <th>Date</th>
                                                    <th>Product</th>
                                                    <th>Quantity</th>
                                                    <th>Price</th>
                                                </tr>
                                                </thead>
                                                <tfoot>
                                                <tr>
                                                    <th>Serial</th>
                                                    <th>Date</th>
                                                    <th>Product</th>
                                                    <th>Quantity</th>
                                                    <th>Price</th>
                                                </tr>
                                                </tfoot>
                                                <tbody>
                                                <?php
                                                $serial= 1;
                                                foreach ($allDataSelected as $oneData){
                                                    $date = date("d/m/Y", strtotime("$oneData->date"));
                                                    ?>
                                                    <tr>
                                                        <td><?php echo $serial?></td>
                                                        <td><?php echo $date?></td>
                                                        <td><?php echo $oneData->prod_name?></td>
                                                        <td><?php echo $oneData->p_quantity." ".$oneData->unit?></td>
                                                        <td><?php echo $oneData->p_price?> /-</td>
                                                    </tr>
                                                    <?php
                                                    $serial++;
                                                }
                                                ?>

                                                </tbody>
                                            </table>
                                            <?php
                                        }
                                        else{
                                            ?>
                                            <h3 style="color: red;text-align: center" >Error! Sorry You selected wrong date format!
                                                From date must be less than to date!
                                            </h3>
                                            <center><a href="purchaseReport.php" class="btn btn-primary">Search Again</a></center>
                                            <?php
                                        }


                                    }
                                    ?>
                                    <?php
                                }
                            else{
                                ?>
                                <center><a class="btn btn-primary" href="printPurchase.php">Print This List</a></center><br>
                                <table id="example" class="table table-bordered table-striped">
                                    <thead>
                                    <tr>
                                        <th>Serial</th>
                                        <th>Date</th>
                                        <th>Product</th>
                                        <th>Quantity</th>
                                        <th>Price</th>
                                    </tr>
                                    </thead>
                                    <tfoot>
                                    <tr>
                                        <th>Serial</th>
                                        <th>Date</th>
                                        <th>Product</th>
                                        <th>Quantity</th>
                                        <th>Price</th>
                                    </tr>
                                    </tfoot>
                                    <tbody>
                                    <?php
                                    $serial= 1;
                                    foreach ($allData as $oneData){
                                        $date = date("d/m/Y", strtotime("$oneData->date"));
                                        ?>
                                        <tr>
                                            <td><?php echo $serial?></td>
                                            <td><?php echo $date?></td>
                                            <td><?php echo $oneData->prod_name?></td>
                                            <td><?php echo $oneData->p_quantity." ".$oneData->unit?></td>
                                            <td><?php echo $oneData->p_price?> /-</td>
                                        </tr>
                                        <?php
                                        $serial++;
                                    }
                                    ?>
                                    </tbody>
                                </table>
                            <?php
                            }
                            ?>
                        </div>
                    </div>
                </div>


            </div>
            <!--inner block end here-->
            <?php require_once ("../templateLayout/footer.php");?>
        </div>
    </div>
    <!--slider menu-->
    <?php require_once ("../templateLayout/managerNavigation.php");?>
    <div class="clearfix"> </div>
</div>
<!--slide bar menu end here-->
<?php require_once ("../templateLayout/templateScript.php")?>
</body>
</html>




