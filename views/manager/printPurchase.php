<?php
session_start();
require_once ("../../vendor/autoload.php");
require_once ("../templateLayout/information.php");
use App\model\Registration_info;
use App\Utility\Utility;
use App\Message\Message;
if($_SESSION['role_status']==1){
    $auth= new Registration_info();
    $status = $auth->prepareData($_SESSION)->logged_in();

    if(!$status) {
        Utility::redirect('../login.php');
        Message::setMessage("Please LogIn first");
        return;
    }
}
else {
    Message::setMessage("Please LogIn first");
    Utility::redirect('../login.php');
}
use App\model\Purchase_details;
$product=new Purchase_details();
if(isset($_POST) && !empty($_POST)){
    $from_date=$_POST['from_date'];
    $to_date=$_POST['to_date'];
    if($from_date<$to_date){
        $product->prepareData($_POST);
        $allDataSelected=$product->showSelectedDate();
    }



}
$allData=$product->showall();
?>
<!DOCTYPE HTML>
<html>
<head>
    <title><?php echo $title?></title>
    <?php require_once ("../templateLayout/templateCss.php");?>
</head>
<body>
<div class="page-container">
    <div class="left-content">
        <div class="mother-grid-inner">
            <?php require_once ("../templateLayout/managerHeader.php")?>
            <div class="inner-block">
                <div class="row" style="min-height: 600px">
                    <?php
                    if(isset($_SESSION) && !empty($_SESSION['message'])) {

                        $msg = Message::getMessage();

                        echo "<p class='help-block' style='color: #0c5577;text-align: center'>$msg</p>";
                    }
                    ?>
                    <div style="width: 52%">
                        <button type="button" onclick="printReceipt()" class="btn btn-primary pull-right"><i class="fa fa-print fa-2x" aria-hidden="true"></i></button>
                    </div>
                     <div id="printArea">
                        <div class="col-md-8 col-md-offset-2">
                            <div class="pro-head">
                                <h2 style="text-align: center">Inventory Management System</h2>
                                <h3 style="text-align: center">Purchase Report</h3><br>
                            </div>
                            <div >
                                <?php
                                if(isset($_POST) && !empty($_POST)){
                                    $date_from = date("d/m/Y", strtotime($from_date));
                                    $date_to = date("d/m/Y", strtotime($to_date));
                                    ?>
                                    <h5 style="text-align: center">From: <?php echo $date_from?> To: <?php echo $date_to?></h5><br>

                                    <table style="border-collapse: collapse;width: 100%" border="2px">
                                        <thead>
                                        <tr>
                                            <th>Serial</th>
                                            <th>Date</th>
                                            <th>Product</th>
                                            <th>Quantity</th>
                                            <th>Price</th>
                                        </tr>
                                        </thead>

                                        <tbody>
                                        <?php
                                        $serial= 1;
                                        foreach ($allDataSelected as $oneData){
                                            $date = date("d/m/Y", strtotime("$oneData->date"));
                                            ?>
                                            <tr>
                                                <td><?php echo $serial?></td>
                                                <td><?php echo $date?></td>
                                                <td><?php echo $oneData->prod_name?></td>
                                                <td><?php echo $oneData->p_quantity." ".$oneData->unit?></td>
                                                <td><?php echo $oneData->p_price?> /-</td>
                                            </tr>
                                            <?php
                                            $serial++;
                                        }
                                        ?>

                                        </tbody>
                                    </table>
                                    <?php
                                }
                                else{
                                    ?>
                                    <table style="border-collapse: collapse;width: 100%" border="2px">
                                        <thead>
                                        <tr>
                                            <th>Serial</th>
                                            <th>Date</th>
                                            <th>Product</th>
                                            <th>Quantity</th>
                                            <th>Price</th>
                                        </tr>
                                        </thead>

                                        <tbody>
                                        <?php
                                        $serial= 1;
                                        foreach ($allData as $oneData){
                                            $date = date("d/m/Y", strtotime("$oneData->date"));
                                            ?>
                                            <tr>
                                                <td><?php echo $serial?></td>
                                                <td><?php echo $date?></td>
                                                <td><?php echo $oneData->prod_name?></td>
                                                <td><?php echo $oneData->p_quantity." ".$oneData->unit?></td>
                                                <td><?php echo $oneData->p_price?> /-</td>
                                            </tr>
                                            <?php
                                            $serial++;
                                        }
                                        ?>
                                        </tbody>
                                    </table>
                                    <?php
                                }
                                ?>
                            </div>
                        </div>
                    </div>

                </div>


            </div>
            <!--inner block end here-->
            <?php require_once ("../templateLayout/footer.php");?>
        </div>
    </div>
    <!--slider menu-->
    <?php require_once ("../templateLayout/managerNavigation.php");?>
    <div class="clearfix"> </div>
</div>
<!--slide bar menu end here-->
<?php require_once ("../templateLayout/templateScript.php")?>
<script>
    function printReceipt()
    {
        var receipt = window.open('','','width=1176,height=1176');
        receipt.document.open("text/html");
        receipt.document.write(document.getElementById('printArea').innerHTML);
        receipt.document.close();
        receipt.print();
    }
</script>
</body>
</html>




