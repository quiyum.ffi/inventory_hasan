<?php
session_start();
require_once ("../../vendor/autoload.php");
require_once ("../templateLayout/information.php");
use App\model\Registration_info;
use App\Utility\Utility;
use App\Message\Message;
if($_SESSION['role_status']==1){
    $auth= new Registration_info();
    $status = $auth->prepareData($_SESSION)->logged_in();

    if(!$status) {
        Utility::redirect('../login.php');
        Message::setMessage("Please LogIn first");
        return;
    }
}
else {
    Message::setMessage("Please LogIn first");
    Utility::redirect('../login.php');
}
use App\model\Product_cat;
use App\model\Unit_lookup;
$unit=new Unit_lookup();
$object=new Product_cat();
$object->prepareData($_SESSION);
$allData=$object->showCategory();
$unit->prepareData($_SESSION);
$unitData=$unit->showUnit();

?>
<!DOCTYPE HTML>
<html>
<head>
    <title><?php echo $title?></title>
    <?php require_once ("../templateLayout/templateCss.php");?>
</head>
<body>
<div class="page-container">
    <div class="left-content">
        <div class="mother-grid-inner">
            <?php require_once ("../templateLayout/managerHeader.php")?>
            <div class="inner-block">
                <div class="row">
                    <?php
                    if(isset($_SESSION) && !empty($_SESSION['message'])) {

                        $msg = Message::getMessage();

                        echo "<p class='help-block' style='color: #0c5577;text-align: center'>$msg</p>";
                    }

                    ?>
                    <div class="col-md-6">
                        <div class="product-block">
                            <div class="pro-head">
                                <h2 style="text-align: center">Products Category</h2>
                            </div>
                            <div class="login-block">

                                <form action="../../controller/manager/addCategory.php" method="post">
                                    <input type="text" name="category_name" placeholder="Category Name" required="">
                                    <input type="hidden" name="admin_id" value="<?php echo $_SESSION['admin_id']?>">
                                    <textarea name="category_desc" class="form-control" placeholder="Category Description"></textarea>
                                    <br>
                                    <input type="submit" value="Add Category">
                                </form>
                            </div>
                            <div class="clearfix"> </div>

                            <div >
                                <div class="pro-head">
                                    <h2 style="text-align: center">Category Details</h2>
                                </div>
                                <table id="example" class="table table-bordered table-striped">
                                    <thead>
                                    <tr>
                                        <th>Serial</th>
                                        <th>Category</th>
                                        <th>Description</th>
                                        <th>Action</th>
                                    </tr>
                                    </thead>
                                    <tfoot>
                                    <tr>
                                        <th>Serial</th>
                                        <th>Category</th>
                                        <th>Description</th>
                                        <th>Action</th>
                                    </tr>
                                    </tfoot>
                                    <tbody>
                                    <?php
                                    $serial= 1;
                                        foreach ($allData as $oneData){

                                            ?>
                                            <tr>
                                                <td><?php echo $serial?></td>
                                                <td><?php echo $oneData->cat_name?></td>
                                                <td><?php echo $oneData->cat_desc?></td>
                                                <td style="text-align: center"><a href='updateCategory.php?id=<?php echo $oneData->id?>' class='btn btn-info'><i class='fa fa-external-link-square ' aria-hidden='true'></i></a>
                                                </td>
                                            </tr>
                                    <?php
                                            $serial++;
                                        }
                                    ?>

                                    </tbody>
                                </table>




                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="product-block">
                            <div class="pro-head">
                                <h2 style="text-align: center">Unit Lookup</h2>
                            </div>
                            <div class="login-block">
                                <form action="../../controller/manager/addUnit.php" method="post">
                                    <input type="text" name="unit" placeholder="Add an Unit" required="">
                                    <input type="hidden" name="admin_id" value="<?php echo $_SESSION['admin_id']?>">
                                    <p style="margin-top: 92px"></p>
                                    <input type="submit" value="Add Unit">
                                </form>
                            </div>
                            <div class="clearfix"> </div>
                            <div>
                                <div class="pro-head">
                                    <h2 style="text-align: center">Unit Details</h2>
                                </div>

                                <table id="example3" class="table table-bordered table-striped">
                                    <thead>
                                    <tr>
                                        <th>Serial</th>
                                        <th>Unit</th>
                                        <th>Action</th>
                                    </tr>
                                    </thead>
                                    <tfoot>
                                    <tr>
                                        <th>Serial</th>
                                        <th>Unit</th>
                                        <th>Action</th>
                                    </tr>
                                    </tfoot>
                                    <tbody>
                                    <?php
                                    $serial= 1;
                                    foreach ($unitData as $oneData){

                                        ?>
                                        <tr>
                                            <td><?php echo $serial?></td>
                                            <td><?php echo $oneData->unit?></td>
                                            <td style="text-align: center"><a href='updateUnit.php?id=<?php echo $oneData->id?>' class='btn btn-info'><i class='fa fa-external-link-square ' aria-hidden='true'></i></a>
                                            </td>
                                        </tr>
                                        <?php
                                        $serial++;
                                    }

                                    ?>
                                    </tbody>
                                </table>




                            </div>
                        </div>
                    </div>
                </div>


            </div>
            <!--inner block end here-->
            <?php require_once ("../templateLayout/footer.php");?>
        </div>
    </div>
    <!--slider menu-->
    <?php require_once ("../templateLayout/managerNavigation.php");?>
    <div class="clearfix"> </div>
</div>
<!--slide bar menu end here-->
<?php require_once ("../templateLayout/templateScript.php")?>
</body>
</html>




