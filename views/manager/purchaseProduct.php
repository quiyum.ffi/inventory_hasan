<?php
session_start();
require_once ("../../vendor/autoload.php");
require_once ("../templateLayout/information.php");
use App\model\Registration_info;
use App\Utility\Utility;
use App\Message\Message;
if($_SESSION['role_status']==1){
    $auth= new Registration_info();
    $status = $auth->prepareData($_SESSION)->logged_in();

    if(!$status) {
        Utility::redirect('../login.php');
        Message::setMessage("Please LogIn first");
        return;
    }
}
else {
    Message::setMessage("Please LogIn first");
    Utility::redirect('../login.php');
}
use App\model\Product;
$product=new Product();
$product->prepareData($_SESSION);
$productData=$product->showProduct();
use App\model\Temp2;
$obj=new Temp2();
$obj->prepareData($_SESSION);
$tempData=$obj->showData();
$vendor=$obj->showVendor();
$status=$obj->is_exist();
$total_sum=$obj->total_price();
if(isset($_GET['id'])){
    $obj->prepareData($_GET);
    $oneItem=$obj->showOneData();
}

?>
<!DOCTYPE HTML>
<html>
<head>
    <title><?php echo $title?></title>
    <?php require_once ("../templateLayout/templateCss.php");?>
</head>
<body>
<div class="page-container">
    <div class="left-content">
        <div class="mother-grid-inner">
            <?php require_once ("../templateLayout/managerHeader.php")?>
            <div class="inner-block">
                <div class="row">

                    <div class="col-md-12">
                        <div class="product-block">
                            <div class="pro-head">
                                <h2 style="text-align: center">Purchase</h2>
                            </div>
                            <?php
                            if(isset($_SESSION) && !empty($_SESSION['message'])) {

                                $msg = Message::getMessage();

                                echo "<p class='help-block' style='color: #0c5577;text-align: center'>$msg</p>";
                            }

                            ?>
                           <div class="col-md-8 col-md-offset-2">
                               <div class="login-block" style="margin-top: -50px;">

                                   <?php
                                    if(isset($_GET['id'])){
                                        ?>
                                        <form action="../../controller/manager/editTempPurchase.php" method="post">
                                            <div class="row">
                                                <div class="col-md-12 col-sm-12 col-xs-12">
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <select name="product_id">

                                                                <?php
                                                                foreach ($productData as $oneData){
                                                                    ?>
                                                                    <option value='<?php echo $oneData->prod_id?>' <?php if($oneData->prod_id==$oneItem->p_id){ echo "selected";}?>><?php echo $oneData->prod_name?></option>
                                                                    <?php
                                                                }
                                                                ?>

                                                            </select>
                                                        </div>
                                                        <div class="col-md-3">
                                                            <input type="number" name="quantity" placeholder="Quantity" value="<?php echo $oneItem->p_quantity?>">
                                                        </div>
                                                        <div class="col-md-3">
                                                            <input type="number" name="price" placeholder="Price" value="<?php echo $oneItem->p_price?>">
                                                            <input type="hidden" name="id" value="<?php echo $oneItem->id?>">
                                                        </div>
                                                    </div><!--//form-group-->
                                                </div>
                                            </div>
                                            <input type="submit" class="btn btn-primary" value="Update" style="width: 40%">
                                        </form>
                                   <?php
                                    }
                                   else{
                                       if($status){
                                           ?>
                                           <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#myModal" style="width: 100%;">Cancel this purchase list & add new list</button>
                                           <br><br>
                                           <div class="modal fade" id="myModal" role="dialog">
                                               <div class="modal-dialog">

                                                   <!-- Modal content-->
                                                   <div class="modal-content">
                                                       <div class="modal-header">
                                                           <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                           <h4 class="modal-title"><?php echo $vendor->vendor_name?></h4>
                                                       </div>
                                                       <div class="modal-body">
                                                           <h4>Are You Sure to delete this list?</h4>
                                                           <a href="../../controller/manager/deletePurchase.php" class="btn btn-primary">Yes</a>
                                                           <a href="" data-dismiss="modal" class="btn btn-danger">No</a>
                                                       </div>
                                                       <div class="modal-footer">
                                                           <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                       </div>
                                                   </div>

                                               </div>
                                           </div>
                                           <?php
                                       }
                                       ?>
                                       <form action="../../controller/manager/tempPurchase.php" method="post">
                                           <div class="row">
                                               <div class="col-md-12 col-sm-12 col-xs-12">
                                                   <div class="row">
                                                       <?php
                                                       if(isset($_GET['vendor_name'])){
                                                           ?>
                                                           <div class="col-md-6">
                                                               <input type="text" name="vendor_name" placeholder="Vendor Name" value="<?php echo $_GET['vendor_name']?>" readonly>
                                                           </div>
                                                           <div class="col-md-6">
                                                               <input type="number" name="vendor_contact" placeholder="Contact Number" value="<?php echo $_GET['contact']?>" readonly>
                                                           </div>
                                                           <?php
                                                       }
                                                       else{
                                                           ?>
                                                           <div class="col-md-6">
                                                               <input type="text" name="vendor_name" placeholder="Vendor Name" value="<?php if($status) echo $vendor->vendor_name?>" <?php if($status) echo "readonly"; else echo "required";?>>
                                                           </div>
                                                           <div class="col-md-6">
                                                               <input type="number" name="vendor_contact" placeholder="Contact Number" value="<?php if($status) echo $vendor->contact?>" <?php if($status) echo "readonly"; else echo "required";?>>
                                                           </div>
                                                           <?php
                                                       }
                                                       ?>


                                                   </div>
                                                   <div class="row">
                                                       <div class="col-md-6">
                                                           <select name="product_id">
                                                               <option value="reject">--Select a Product--</option>
                                                               <?php
                                                               foreach ($productData as $oneData){
                                                                   echo "<option value='$oneData->prod_id'>$oneData->prod_name</option>";
                                                               }
                                                               ?>

                                                           </select>
                                                       </div>
                                                       <div class="col-md-3">
                                                           <input type="number" name="quantity" placeholder="Quantity" required>
                                                       </div>
                                                       <div class="col-md-3">
                                                           <input type="number" name="price" placeholder="Price" required>
                                                       </div>
                                                   </div><!--//form-group-->
                                               </div>
                                           </div>
                                           <input type="hidden" name="admin_id" value="<?php echo $_SESSION['admin_id']?>">
                                           <input type="submit" class="btn btn-primary" value="Add Another" style="width: 40%">
                                       </form>
                                   <?php
                                   }
                                   ?>


                               </div>
                           </div>
                            <div class="clearfix"> </div>
                        </div>

                    </div>
                    <div class="col-md-12">
                        <div class="col-md-11 ">
                            <div class="pro-head">
                                <h2 style="text-align: center">Draft</h2>
                            </div>
                            <div class="row">

                                <form action="../../controller/manager/purchase.php" method="post">
                                    <table id="example" class="table table-bordered table-striped" >
                                        <thead>
                                        <tr>
                                            <th>Serial</th>
                                            <th>Product</th>
                                            <th>Quantity</th>
                                            <th>Unit</th>
                                            <th>Price</th>
                                            <th>Total</th>
                                            <th>Edit</th>
                                            <th>Delete</th>
                                        </tr>
                                        </thead>
                                        <tfoot>
                                        <tr>
                                            <th>Serial</th>
                                            <th>Product</th>
                                            <th>Quantity</th>
                                            <th>Unit</th>
                                            <th>Price</th>
                                            <th>Total</th>
                                            <th>Edit</th>
                                            <th>Delete</th>
                                        </tr>
                                        </tfoot>
                                        <tbody>

                                        <?php
                                        $serial=1;
                                        foreach ($tempData as $oneData){
                                            echo"
                                    <tr>
                                        <td>$serial</td>      
                                        <td><input type='text' value='$oneData->prod_name' readonly> <input type='hidden' name='p_id[]' value='$oneData->p_id'></td>
                                        <td><input type='text' name='quantity[]' value='$oneData->p_quantity'  readonly></td>
                                        <td><input type='text' value='$oneData->unit' readonly></td>
                                        <td><input type='text' name='price[]' value='$oneData->p_price' readonly></td>
                                        <td><input name='total[]' value='$oneData->total' readonly></td>
                                       
                                        <td><a href='purchaseProduct.php?id=$oneData->id' class='btn btn-info'><i class='fa fa-external-link-square ' aria-hidden='true'></i></a></td>
                                        <td><a href='../../controller/manager/deletePurchaseProduct.php?id=$oneData->id' class='btn btn-danger'><i class='fa fa-trash-o' aria-hidden='true'></i></a></td>
                                    
                                    </tr>
                                ";
                                            $serial++;
                                        }
                                        ?>



                                        </tbody>
                                    </table>

                                    <?php
                                    if($status){
                                        ?>
                                        <div class="login-block">
                                            <div class="col-md-4">
                                                <input type="text" name="total_sum" value="<?php echo $total_sum->total_sum?>" readonly>
                                                <input type="hidden" name="vendor_name" value="<?php echo $vendor->vendor_name?>">
                                                <input type="hidden" name="vendor_contact" value="<?php echo $vendor->contact?>">
                                                <input type="hidden" name="admin_id" value="<?php echo $vendor->admin_id?>">
                                            </div>
                                            <div class="col-md-4">
                                                <input type="number" name="paid" placeholder="Paid Amount" required>
                                            </div>
                                            <div class="col-md-4">
                                                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal2" style="width: 100%;">Purchase</button>
                                            </div>
                                        </div>
                                        <?php
                                    }
                                    ?>
                                    <div class="modal fade" id="myModal2" role="dialog">
                                        <div class="modal-dialog">

                                            <!-- Modal content-->
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                    <h4 class="modal-title"><?php echo $vendor->vendor_name?></h4>
                                                </div>
                                                <div class="modal-body">
                                                    <h4>Are You Sure to purchase this list?</h4>
                                                    <input type="submit" class="btn btn-primary" value="Yes">
                                                    <a href="" data-dismiss="modal" class="btn btn-danger">No</a>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </form>

                            </div>
                        </div>
                    </div>
                </div>


            </div>
            <!--inner block end here-->
            <?php require_once ("../templateLayout/footer.php");?>
        </div>
    </div>
    <!--slider menu-->
    <?php require_once ("../templateLayout/managerNavigation.php");?>
    <div class="clearfix"> </div>
</div>
<!--slide bar menu end here-->

<?php require_once ("../templateLayout/templateScript.php")?>
</body>
</html>
