-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Aug 26, 2017 at 05:53 PM
-- Server version: 10.2.3-MariaDB-log
-- PHP Version: 7.1.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `inventory`
--

-- --------------------------------------------------------

--
-- Table structure for table `bill_details`
--

CREATE TABLE `bill_details` (
  `id` int(11) NOT NULL,
  `bill_no` int(11) NOT NULL,
  `prod_id` int(11) NOT NULL,
  `quantity` int(11) NOT NULL,
  `prod_price` int(11) NOT NULL,
  `total` int(11) NOT NULL,
  `admin_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bill_details`
--

INSERT INTO `bill_details` (`id`, `bill_no`, `prod_id`, `quantity`, `prod_price`, `total`, `admin_id`) VALUES
(37, 17, 37, 1, 1200, 1200, 2),
(38, 18, 41, 4, 1200, 4800, 3),
(39, 19, 37, 1, 1111, 1111, 2);

-- --------------------------------------------------------

--
-- Table structure for table `bill_master`
--

CREATE TABLE `bill_master` (
  `bill_no` int(11) NOT NULL,
  `date` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `cus_id` int(11) NOT NULL,
  `total_amount` int(11) NOT NULL,
  `paid` int(11) NOT NULL,
  `due` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `admin_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bill_master`
--

INSERT INTO `bill_master` (`bill_no`, `date`, `cus_id`, `total_amount`, `paid`, `due`, `status`, `admin_id`) VALUES
(17, '2017-08-24 09:39:25', 20, 1200, 1200, 0, 0, 2),
(18, '2017-08-24 10:14:59', 21, 4800, 4800, 0, 0, 3),
(19, '2017-08-25 19:20:17', 22, 1111, 1111, 0, 0, 2);

-- --------------------------------------------------------

--
-- Table structure for table `customer`
--

CREATE TABLE `customer` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `contact` varchar(15) NOT NULL,
  `date` datetime NOT NULL DEFAULT current_timestamp(),
  `status` int(11) NOT NULL DEFAULT 0,
  `admin_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `customer`
--

INSERT INTO `customer` (`id`, `name`, `contact`, `date`, `status`, `admin_id`) VALUES
(19, 'Harun', '01866322186', '2017-08-24 15:25:09', 0, 2),
(20, 'Harun', '01826132308', '2017-08-24 15:39:25', 0, 2),
(21, 'Harun', '01827283029', '2017-08-24 16:14:59', 0, 3),
(22, 'Harun', '01826132308', '2017-08-26 01:20:17', 0, 2);

-- --------------------------------------------------------

--
-- Stand-in structure for view `inventory_1`
-- (See below for the actual view)
--
CREATE TABLE `inventory_1` (
`prod_id` int(11)
,`dr_quantity` bigint(20)
,`cr_quantity` bigint(20)
,`admin_id` int(11)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `inventory_final`
-- (See below for the actual view)
--
CREATE TABLE `inventory_final` (
`prod_id` int(11)
,`prod_name` varchar(255)
,`dr_qty` decimal(41,0)
,`cr_qty` decimal(41,0)
,`bal_qty` decimal(42,0)
,`admin_id` int(11)
);

-- --------------------------------------------------------

--
-- Table structure for table `manager`
--

CREATE TABLE `manager` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `contact` varchar(15) NOT NULL,
  `user_name` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `assign_date` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `picture` varchar(255) NOT NULL DEFAULT 'manager.jpg',
  `status` int(1) NOT NULL DEFAULT 1,
  `admin_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `manager`
--

INSERT INTO `manager` (`id`, `name`, `contact`, `user_name`, `password`, `assign_date`, `picture`, `status`, `admin_id`) VALUES
(6, 'Arman', '01811000000', 'arman', 'e10adc3949ba59abbe56e057f20f883e', '2017-08-24 12:42:27', '1503578526samir.jpg', 1, 3),
(7, 'Harun', '01811000000', 'harun123', 'e10adc3949ba59abbe56e057f20f883e', '2017-08-24 14:43:14', 'manager.jpg', 1, 2);

-- --------------------------------------------------------

--
-- Table structure for table `opening_product`
--

CREATE TABLE `opening_product` (
  `id` int(11) NOT NULL,
  `admin_id` int(11) NOT NULL,
  `prod_id` int(11) NOT NULL,
  `quantity` int(11) NOT NULL,
  `buy_price` int(11) NOT NULL,
  `date` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `opening_product`
--

INSERT INTO `opening_product` (`id`, `admin_id`, `prod_id`, `quantity`, `buy_price`, `date`) VALUES
(18, 2, 37, 2, 4500, '2017-08-23 02:11:04'),
(19, 2, 39, 5, 4578, '2017-08-23 02:11:47'),
(21, 2, 40, 4, 1700, '2017-08-23 02:32:19'),
(22, 3, 41, 12, 20, '2017-08-24 16:13:35'),
(23, 4, 42, 45, 32, '2017-08-24 18:47:41'),
(24, 4, 43, 90, 30, '2017-08-24 18:48:02'),
(25, 4, 44, 20, 1890, '2017-08-24 18:48:19');

-- --------------------------------------------------------

--
-- Table structure for table `product`
--

CREATE TABLE `product` (
  `prod_id` int(11) NOT NULL,
  `admin_id` int(11) NOT NULL,
  `prod_name` varchar(255) NOT NULL,
  `unit_id` int(11) NOT NULL,
  `cat_id` int(11) NOT NULL,
  `date` datetime NOT NULL DEFAULT current_timestamp(),
  `status` int(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `product`
--

INSERT INTO `product` (`prod_id`, `admin_id`, `prod_name`, `unit_id`, `cat_id`, `date`, `status`) VALUES
(29, 0, 'Rice', 4, 3, '2017-08-05 13:06:38', 0),
(30, 0, 'Rice (per packet)', 1, 3, '2017-08-05 13:07:04', 0),
(31, 0, 'Salt', 4, 3, '2017-08-09 11:33:52', 0),
(32, 0, 'potato', 4, 6, '2017-08-16 05:09:48', 0),
(33, 0, 'Masala', 4, 6, '2017-08-22 16:17:32', 0),
(34, 0, 'Oil (1 Liter)', 2, 6, '2017-08-22 17:32:44', 0),
(35, 0, 'Oil (2 litre)', 2, 6, '2017-08-22 17:33:03', 0),
(36, 0, 'Oil (5 litre)', 2, 6, '2017-08-22 17:33:16', 0),
(37, 2, 'Mobile Huawei Y6 pro', 13, 7, '2017-08-23 01:38:26', 0),
(38, 3, 'Light', 15, 9, '2017-08-23 01:41:20', 0),
(39, 2, 'Huawei Y3c', 13, 7, '2017-08-23 02:11:35', 0),
(40, 2, 'Hard Disk', 13, 7, '2017-08-23 02:14:27', 0),
(41, 3, 'Rice (50kg)', 15, 8, '2017-08-24 16:13:20', 0),
(42, 4, 'Potato', 16, 10, '2017-08-24 18:45:39', 0),
(43, 4, 'Rice', 16, 10, '2017-08-24 18:45:48', 0),
(44, 4, 'Rice (50kg)', 18, 10, '2017-08-24 18:45:55', 0),
(45, 4, 'Oil (1 Liter)', 19, 10, '2017-08-24 18:47:13', 0),
(46, 4, 'Oil (2 litre)', 19, 10, '2017-08-24 18:47:22', 0),
(47, 4, 'Oil (5 litre)', 19, 10, '2017-08-24 18:47:29', 0);

-- --------------------------------------------------------

--
-- Table structure for table `product_cat`
--

CREATE TABLE `product_cat` (
  `id` int(11) NOT NULL,
  `admin_id` int(11) NOT NULL,
  `cat_name` varchar(200) NOT NULL,
  `cat_desc` text NOT NULL,
  `date` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `cat_status` varchar(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `product_cat`
--

INSERT INTO `product_cat` (`id`, `admin_id`, `cat_name`, `cat_desc`, `date`, `cat_status`) VALUES
(7, 2, 'Hardware', 'hardware', '2017-08-22 19:12:59', '0'),
(8, 3, 'dfdf', 'dfdfdf', '2017-08-22 18:00:00', '0'),
(9, 3, 'Hardware', 'Hardware', '2017-08-22 19:30:59', '0'),
(10, 4, 'Grocerry', 'grocery', '2017-08-24 12:45:10', '0'),
(11, 2, 'Grocery', 'Grocery', '2017-08-26 07:35:34', '0');

-- --------------------------------------------------------

--
-- Table structure for table `purchase_bill`
--

CREATE TABLE `purchase_bill` (
  `id` int(11) NOT NULL,
  `purchase_master_id` int(11) NOT NULL,
  `payment` int(11) NOT NULL,
  `date` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `purchase_bill`
--

INSERT INTO `purchase_bill` (`id`, `purchase_master_id`, `payment`, `date`) VALUES
(1, 25, 2000, '2017-07-29 10:35:14'),
(2, 25, 2000, '2017-07-29 10:40:58'),
(3, 25, 20000, '2017-07-29 10:41:32'),
(4, 25, 1020, '2017-07-29 10:49:19'),
(5, 24, 100, '2017-07-30 06:24:27'),
(6, 24, 200, '2017-07-30 06:25:06'),
(7, 26, 10000, '2017-07-30 06:29:23'),
(8, 27, 4000, '2017-07-30 06:43:35'),
(9, 27, 450, '2017-07-30 06:44:47'),
(10, 27, 9000, '2017-07-30 06:45:50'),
(11, 27, 550, '2017-07-30 06:46:14'),
(12, 24, 1700, '2017-07-30 06:48:41'),
(13, 29, 12000, '2017-08-05 07:11:33'),
(14, 29, 1540, '2017-08-05 14:27:29'),
(15, 30, 200, '2017-08-05 14:28:39'),
(16, 30, 1000, '2017-08-05 14:29:14'),
(17, 30, 1000, '2017-08-05 14:29:23'),
(18, 31, 1200, '2017-08-05 14:35:32'),
(19, 32, 500, '2017-08-05 19:55:42'),
(20, 33, 52500, '2017-08-12 19:24:42'),
(21, 34, 600, '2017-08-12 19:25:27'),
(22, 35, 200, '2017-08-15 22:01:24'),
(23, 35, 11000, '2017-08-15 22:01:44'),
(24, 36, 1054, '2017-08-22 11:08:02'),
(25, 31, 20000, '2017-08-22 11:29:25'),
(26, 37, 3168, '2017-08-22 11:41:57'),
(27, 38, 190000, '2017-08-22 20:57:17'),
(28, 38, 500, '2017-08-22 20:59:37'),
(29, 39, 4545, '2017-08-22 21:01:45'),
(30, 39, 185955, '2017-08-22 21:02:06'),
(31, 40, 145500, '2017-08-24 09:34:32'),
(32, 41, 12000, '2017-08-24 10:14:12'),
(33, 42, 161600, '2017-08-24 11:15:16'),
(34, 43, 22750, '2017-08-24 11:16:20'),
(35, 44, 32198, '2017-08-24 15:21:28'),
(36, 45, 43200, '2017-08-25 19:27:27'),
(37, 46, 14400, '2017-08-25 20:17:49');

-- --------------------------------------------------------

--
-- Table structure for table `purchase_details`
--

CREATE TABLE `purchase_details` (
  `id` int(11) NOT NULL,
  `mrr_no` int(11) NOT NULL,
  `p_id` int(11) NOT NULL,
  `p_quantity` int(11) NOT NULL,
  `p_price` int(11) NOT NULL,
  `total` int(11) NOT NULL,
  `admin_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `purchase_details`
--

INSERT INTO `purchase_details` (`id`, `mrr_no`, `p_id`, `p_quantity`, `p_price`, `total`, `admin_id`) VALUES
(47, 44, 37, 2, 11500, 23000, 2),
(48, 44, 39, 2, 4599, 9198, 2),
(49, 45, 37, 12, 1200, 14400, 2),
(50, 45, 39, 24, 1200, 28800, 2),
(51, 46, 41, 12, 1200, 14400, 3);

-- --------------------------------------------------------

--
-- Table structure for table `purchase_master`
--

CREATE TABLE `purchase_master` (
  `mrr_no` int(11) NOT NULL,
  `date` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `vendor_name` varchar(255) NOT NULL,
  `contact` varchar(15) NOT NULL,
  `total_amount` int(11) NOT NULL,
  `paid` int(11) NOT NULL,
  `due` int(11) NOT NULL,
  `status` int(11) NOT NULL DEFAULT 1,
  `admin_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `purchase_master`
--

INSERT INTO `purchase_master` (`mrr_no`, `date`, `vendor_name`, `contact`, `total_amount`, `paid`, `due`, `status`, `admin_id`) VALUES
(44, '2017-08-24 15:21:28', 'Abdul Hamid', '01858566666', 32198, 32198, 0, 0, 2),
(45, '2017-08-25 19:27:26', 'Abdul Hamid', '01858566666', 43200, 43200, 0, 0, 2),
(46, '2017-08-25 20:17:49', 'Anuwar', '0182634567', 14400, 14400, 0, 0, 3);

-- --------------------------------------------------------

--
-- Table structure for table `registration_info`
--

CREATE TABLE `registration_info` (
  `id` int(11) NOT NULL,
  `name` varchar(200) NOT NULL,
  `email` varchar(200) NOT NULL,
  `contact` varchar(200) NOT NULL,
  `user_name` varchar(200) NOT NULL,
  `password` varchar(500) NOT NULL,
  `picture` varchar(255) NOT NULL DEFAULT 'manager.jpg',
  `reg_date` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `status` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `registration_info`
--

INSERT INTO `registration_info` (`id`, `name`, `email`, `contact`, `user_name`, `password`, `picture`, `reg_date`, `status`) VALUES
(1, 'Abdullah al Quiyum', 'quiyum.nyn@gmail.com', '01826132308', 'quiyum', 'e10adc3949ba59abbe56e057f20f883e', 'manager.jpg', '2017-07-04 21:26:03', 0),
(2, 'Hasan Uddin', 'hasanuddin808055@gmail.com', '01814081800', 'hasan', 'fcea920f7412b5da7be0cf42b8c93759', '1502904224adil jr.jpg', '2017-08-16 17:30:02', 0),
(3, 'Kabil', 'a@gmail.com', '01814081800', 'kabil', 'e10adc3949ba59abbe56e057f20f883e', 'manager.jpg', '2017-08-22 10:14:37', 0),
(4, 'Badrul Hasan Tusar', 'badrul@gmail.com', '01866322186', 'badrul', 'e10adc3949ba59abbe56e057f20f883e', 'manager.jpg', '2017-08-24 12:43:45', 0);

-- --------------------------------------------------------

--
-- Table structure for table `sale_bill`
--

CREATE TABLE `sale_bill` (
  `id` int(11) NOT NULL,
  `bill_master_id` int(11) NOT NULL,
  `payment` int(11) NOT NULL,
  `total_payment` int(11) NOT NULL,
  `date` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sale_bill`
--

INSERT INTO `sale_bill` (`id`, `bill_master_id`, `payment`, `total_payment`, `date`) VALUES
(4, 7, 20000, 0, '2017-08-06 08:22:03'),
(5, 8, 44850, 0, '2017-08-06 08:24:45'),
(6, 7, 1000, 0, '2017-08-06 19:33:02'),
(7, 7, 4000, 0, '2017-08-06 19:33:21'),
(8, 10, 1000, 1000, '2017-08-08 19:59:03'),
(9, 10, 1000, 2000, '2017-08-08 20:06:27'),
(10, 11, 276, 276, '2017-08-22 11:08:31'),
(11, 12, 276, 276, '2017-08-22 11:10:33'),
(12, 13, 690, 690, '2017-08-22 11:43:12'),
(13, 14, 11000, 11000, '2017-08-24 09:17:52'),
(15, 16, 12000, 12000, '2017-08-24 09:25:10'),
(16, 17, 1200, 1200, '2017-08-24 09:39:25'),
(17, 18, 4800, 4800, '2017-08-24 10:15:00'),
(18, 19, 1111, 1111, '2017-08-25 19:20:18');

-- --------------------------------------------------------

--
-- Stand-in structure for view `stock2`
-- (See below for the actual view)
--
CREATE TABLE `stock2` (
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `stock_final`
-- (See below for the actual view)
--
CREATE TABLE `stock_final` (
);

-- --------------------------------------------------------

--
-- Table structure for table `superadmin`
--

CREATE TABLE `superadmin` (
  `id` int(11) NOT NULL,
  `userName` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `status` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `superadmin`
--

INSERT INTO `superadmin` (`id`, `userName`, `password`, `status`) VALUES
(1, 'azad', '36', 0);

-- --------------------------------------------------------

--
-- Table structure for table `temp`
--

CREATE TABLE `temp` (
  `id` int(11) NOT NULL,
  `p_id` int(11) NOT NULL,
  `quantity` int(11) NOT NULL,
  `prod_price` int(11) NOT NULL,
  `total` int(11) NOT NULL,
  `customer_name` varchar(255) NOT NULL,
  `contact` varchar(255) NOT NULL,
  `admin_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `temp2`
--

CREATE TABLE `temp2` (
  `id` int(11) NOT NULL,
  `p_id` int(11) NOT NULL,
  `p_quantity` int(11) NOT NULL,
  `p_price` int(11) NOT NULL,
  `vendor_name` varchar(255) NOT NULL,
  `contact` varchar(15) NOT NULL,
  `total` int(11) NOT NULL,
  `admin_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `temp3`
--

CREATE TABLE `temp3` (
  `acc_id` int(11) NOT NULL,
  `acc_name` varchar(255) NOT NULL,
  `cost` int(11) NOT NULL,
  `remarks` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `unit_lookup`
--

CREATE TABLE `unit_lookup` (
  `id` int(11) NOT NULL,
  `admin_id` int(11) NOT NULL,
  `unit` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `unit_lookup`
--

INSERT INTO `unit_lookup` (`id`, `admin_id`, `unit`) VALUES
(1, 0, 'pcs'),
(2, 0, 'litre'),
(3, 0, 'meter'),
(4, 0, 'kg'),
(6, 0, 'role'),
(7, 0, 'Inch'),
(9, 0, 'km'),
(10, 0, 'MB'),
(11, 0, 'Set'),
(13, 2, 'pcs'),
(14, 2, 'Kg'),
(15, 3, 'pcs'),
(16, 4, 'Kg'),
(17, 4, 'grm'),
(18, 4, 'pcs'),
(19, 4, 'Liter'),
(20, 2, 'gm');

-- --------------------------------------------------------

--
-- Structure for view `inventory_1`
--
DROP TABLE IF EXISTS `inventory_1`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `inventory_1`  AS  select `opening_product`.`prod_id` AS `prod_id`,`opening_product`.`quantity` AS `dr_quantity`,0 AS `cr_quantity`,`opening_product`.`admin_id` AS `admin_id` from `opening_product` where `opening_product`.`quantity` <> 0 union all select `purchase_details`.`p_id` AS `prod_id`,`purchase_details`.`p_quantity` AS `dr_quantity`,0 AS `cr_quantity`,`purchase_details`.`admin_id` AS `admin_id` from `purchase_details` union all select `bill_details`.`prod_id` AS `prod_id`,0 AS `dr_quantity`,`bill_details`.`quantity` AS `cr_quantity`,`bill_details`.`admin_id` AS `admin_id` from `bill_details` ;

-- --------------------------------------------------------

--
-- Structure for view `inventory_final`
--
DROP TABLE IF EXISTS `inventory_final`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `inventory_final`  AS  select `a`.`prod_id` AS `prod_id`,`b`.`prod_name` AS `prod_name`,sum(`a`.`dr_quantity`) AS `dr_qty`,sum(`a`.`cr_quantity`) AS `cr_qty`,sum(`a`.`dr_quantity`) - sum(`a`.`cr_quantity`) AS `bal_qty`,`a`.`admin_id` AS `admin_id` from (`inventory_1` `a` join `product` `b`) where `a`.`prod_id` = `b`.`prod_id` group by `a`.`prod_id` ;

-- --------------------------------------------------------

--
-- Structure for view `stock2`
--
DROP TABLE IF EXISTS `stock2`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `stock2`  AS  select `a`.`prod_id` AS `prod_id`,`b`.`prod_name` AS `prod_name`,sum(`a`.`dr_quantity`) AS `dr_qty`,sum(`a`.`cr_quantity`) AS `cr_qty`,sum(`a`.`dr_quantity`) - sum(`a`.`cr_quantity`) AS `bal_qty` from (`stock1` `a` join `product_master` `b`) where `a`.`prod_id` = `b`.`prod_id` group by `a`.`prod_id` ;

-- --------------------------------------------------------

--
-- Structure for view `stock_final`
--
DROP TABLE IF EXISTS `stock_final`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `stock_final`  AS  select `a`.`prod_id` AS `prod_id`,`b`.`prod_name` AS `prod_name`,sum(`a`.`dr_quantity`) AS `dr_qty`,sum(`a`.`cr_quantity`) AS `cr_qty`,sum(`a`.`dr_quantity`) - sum(`a`.`cr_quantity`) AS `bal_qty` from (`stock1` `a` join `product` `b`) where `a`.`prod_id` = `b`.`prod_id` group by `a`.`prod_id` ;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `bill_details`
--
ALTER TABLE `bill_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bill_master`
--
ALTER TABLE `bill_master`
  ADD PRIMARY KEY (`bill_no`);

--
-- Indexes for table `customer`
--
ALTER TABLE `customer`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `manager`
--
ALTER TABLE `manager`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `opening_product`
--
ALTER TABLE `opening_product`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`prod_id`);

--
-- Indexes for table `product_cat`
--
ALTER TABLE `product_cat`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `purchase_bill`
--
ALTER TABLE `purchase_bill`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `purchase_details`
--
ALTER TABLE `purchase_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `purchase_master`
--
ALTER TABLE `purchase_master`
  ADD PRIMARY KEY (`mrr_no`);

--
-- Indexes for table `registration_info`
--
ALTER TABLE `registration_info`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sale_bill`
--
ALTER TABLE `sale_bill`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `superadmin`
--
ALTER TABLE `superadmin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `temp`
--
ALTER TABLE `temp`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `temp2`
--
ALTER TABLE `temp2`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `temp3`
--
ALTER TABLE `temp3`
  ADD UNIQUE KEY `acc_id` (`acc_id`);

--
-- Indexes for table `unit_lookup`
--
ALTER TABLE `unit_lookup`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `bill_details`
--
ALTER TABLE `bill_details`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40;
--
-- AUTO_INCREMENT for table `bill_master`
--
ALTER TABLE `bill_master`
  MODIFY `bill_no` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT for table `customer`
--
ALTER TABLE `customer`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;
--
-- AUTO_INCREMENT for table `manager`
--
ALTER TABLE `manager`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `opening_product`
--
ALTER TABLE `opening_product`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;
--
-- AUTO_INCREMENT for table `product`
--
ALTER TABLE `product`
  MODIFY `prod_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=48;
--
-- AUTO_INCREMENT for table `product_cat`
--
ALTER TABLE `product_cat`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `purchase_bill`
--
ALTER TABLE `purchase_bill`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;
--
-- AUTO_INCREMENT for table `purchase_details`
--
ALTER TABLE `purchase_details`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=52;
--
-- AUTO_INCREMENT for table `purchase_master`
--
ALTER TABLE `purchase_master`
  MODIFY `mrr_no` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=47;
--
-- AUTO_INCREMENT for table `registration_info`
--
ALTER TABLE `registration_info`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `sale_bill`
--
ALTER TABLE `sale_bill`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT for table `superadmin`
--
ALTER TABLE `superadmin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `temp`
--
ALTER TABLE `temp`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `temp2`
--
ALTER TABLE `temp2`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `unit_lookup`
--
ALTER TABLE `unit_lookup`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
